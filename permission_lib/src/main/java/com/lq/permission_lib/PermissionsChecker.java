package com.lq.permission_lib;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * 检查权限的工具类
 * <p/>
 * Created by wangchenlong on 16/1/26.
 */
public class PermissionsChecker {

//       <uses-permission android:name="android.permission.WRITE_SETTINGS" />
//    <uses-permission android:name="android.permission.INTERNET" />
//    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
//    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
//    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
//    <uses-permission android:name="android.permission.WAKE_LOCK" />
//    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
//    <uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />
//    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
//    <uses-permission android:name="android.permission.GET_TASKS" />
//    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
   public static final String[] PERMISSIONS = new String[]{
           Manifest.permission.INTERNET,
           Manifest.permission.WRITE_EXTERNAL_STORAGE,
           Manifest.permission.ACCESS_NETWORK_STATE,
           Manifest.permission.READ_EXTERNAL_STORAGE,
           Manifest.permission.ACCESS_COARSE_LOCATION,
           Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.WAKE_LOCK,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.CHANGE_NETWORK_STATE,
        Manifest.permission.CHANGE_WIFI_STATE,
        Manifest.permission.RECEIVE_BOOT_COMPLETED
    };

    // 判断权限集合
    public static boolean lacksPermissions(Context c,String... permissions) {
        for (String permission : permissions) {
            if (lacksPermission(c,permission)) {
                return true;
            }
        }
        return false;
    }

    // 判断是否缺少权限
    public static boolean lacksPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) ==
                PackageManager.PERMISSION_DENIED;
    }
}
