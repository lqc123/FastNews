#include <jni.h>
//#include <String.h>
//const static char *API_HOST = "http://apis.baidu.com/";

// static int API_KEY=0;
//const int API_KEY_BUS=1;
//const int VIDEO_KEY=2;
//const int TODAY_HISTORY_KEY=3;


/**
 *
    public static final String API_HOST = "http://apis.baidu.com/";
    public static final String API_KEY = "70a30f59c0c605a7837c7b211aa88f3c";
    public static final String IMAGE = "http://image.baidu.com/data/imgs";

//    请求地址：http://op.juhe.cn/onebox/weather/query
//    请求参数：cityname=%E5%90%88%E8%82%A5&dtype=json&key=53bebb554ce5ca0ba47f79f88833866a
//    请求方式：GET
    public static final String API_HOST_WEATHER = "http://op.juhe.cn/";
    public static final String API_KEY_WEATHER = "53bebb554ce5ca0ba47f79f88833866a";

//    请求地址：http://op.juhe.cn/189/bus/busline
//    请求参数：dtype=json&city=%E5%90%88%E8%82%A5&bus=902&key=299d18c4f75732b7d9b82f1868c01dfb
//    请求方式：GET
    public static final String API_HOST_BUS = "http://op.juhe.cn/";
    public static final String API_KEY_BUS = "299d18c4f75732b7d9b82f1868c01dfb";
    public static final String VIDEO="http://op.juhe.cn/onebox/movie/pmovie";
    public static final  String VIDEO_KEY="608c12182c5efe586b184f887451caff";
    public static final String TODAY_HISTORY="http://api.juheapi.com/japi/toh";
    public static final String TODAY_HISTORY_KEY="e7e495cd446fa33eb8be3405c715b1dd";
 */
JNIEXPORT jstring JNICALL Java_com_lq_fastnews_model_api_ApiDefine_api
  (JNIEnv *env, jobject obj, jint type){
	char *key;
	switch(type){
	case 0:
		key="70a30f59c0c605a7837c7b211aa88f3c";
		break;
	case 1:
		key="53bebb554ce5ca0ba47f79f88833866a";
		break;


	case 2:
			key="299d18c4f75732b7d9b82f1868c01dfb";
			break;
	case 3:
		key="608c12182c5efe586b184f887451caff";
		break;

	case 4:
			key="e7e495cd446fa33eb8be3405c715b1dd";
			break;
	}


	return (*env)->NewStringUTF(env, key);

}
