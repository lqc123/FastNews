package com.lq.fastnews.model.subscribe;

import rx.Subscriber;

/**
 * Created by Administrator on 2016/6/9.
 */
public abstract class TaskSubsriber<T> extends Subscriber<T> {
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

}
