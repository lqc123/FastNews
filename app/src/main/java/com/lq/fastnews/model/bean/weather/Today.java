package com.lq.fastnews.model.bean.weather;

import java.util.List;

/**
 * Created by Administrator on 2016/5/22.
 */
public class Today {

    /**
     * aqi : 117
     * curTemp : 28℃
     * date : 2016-05-22
     * fengli : 微风级
     * fengxiang : 无持续风向
     * hightemp : 29℃
     * index : [{"code":"gm","details":"各项气象条件适宜，无明显降温过程，发生感冒机率较低。","index":"","name":"感冒指数","otherName":""},{"code":"fs","details":"属强紫外辐射天气，外出时应加强防护，建议涂擦SPF在15-20之间，PA++的防晒护肤品。","index":"强","name":"防晒指数","otherName":""},{"code":"ct","details":"天气热，建议着短裙、短裤、短薄外套、T恤等夏季服装。","index":"热","name":"穿衣指数","otherName":""},{"code":"yd","details":"天气较好，户外运动请注意防晒。推荐您进行室内运动。","index":"较适宜","name":"运动指数","otherName":""},{"code":"xc","details":"较适宜洗车，未来一天无雨，风力较小，擦洗一新的汽车至少能保持一天。","index":"较适宜","name":"洗车指数","otherName":""},{"code":"ls","details":"天气不错，极适宜晾晒。抓紧时机把久未见阳光的衣物搬出来晒晒太阳吧！","index":"极适宜","name":"晾晒指数","otherName":""}]
     * lowtemp : 18℃
     * type : 晴
     * week : 星期天
     */

    public String aqi;
    public String curTemp;
    public String date;
    public String fengli;
    public String fengxiang;
    public String hightemp;
    public String lowtemp;
    public String type;
    public String week;
    /**
     * code : gm
     * details : 各项气象条件适宜，无明显降温过程，发生感冒机率较低。
     * index :
     * name : 感冒指数
     * otherName :
     */

    public List<IndexBean> index;



    public static class IndexBean {
        public String code;
        public String details;
        public String index;
        public String name;
        public String otherName;

    }
}
