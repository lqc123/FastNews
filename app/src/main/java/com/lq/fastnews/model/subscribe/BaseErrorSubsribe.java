package com.lq.fastnews.model.subscribe;

import android.text.TextUtils;
import android.view.View;

import com.lq.fastnews.iview.LoadView;
import com.lq.fastnews.utils.netstatus.NetWorkUtil;

/**
 * Created by Administrator on 2016/6/28.
 */
public abstract class BaseErrorSubsribe<T,V extends LoadView> extends BaseSubsribe<T,V> implements View.OnClickListener {
    public BaseErrorSubsribe(V view) {
        super(view);
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        if(NetWorkUtil.isNetAndShow()){
            mView.showError("",this, LoadView.ERROR);
        }else {
            mView.showError("",this, LoadView.NET_ERROR);
        }
    }

    @Override
    public void onNext(T t) {
        if(TextUtils.isEmpty(t.toString())){
            mView.showError("",null, LoadView.EMPTY);
            return;
        }
        super.onNext(t);
    }


    @Override
    public void onClick(View v) {
        if(mView!=null)
        reLoad();
    }

    protected abstract void reLoad();
}
