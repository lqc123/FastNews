package com.lq.fastnews.model.bean.image;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/10 0010.
 */
public class ImageInfo implements Serializable {

    public String title;
    public String desc;
    public String thumbLargeUrl;

    @Override
    public String toString() {
        return "ImageInfo{" +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", thumbLargeUrl='" + thumbLargeUrl + '\'' +
                '}';
    }


}
