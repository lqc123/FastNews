package com.lq.fastnews.model.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/6/29.
 */
public class TodyHistory {

    /**
     * error_code : 0
     * reason : 请求成功！
     * result : [{"_id":"10850101","day":1,"des":"在931年前的今天，1085年1月1日 (农历腊月初三)，司马光主持编撰的《资治通鉴》成书。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200911/30/0F02714956.jpg","title":"司马光主持编撰的《资治通鉴》成书","year":1085},{"_id":"18140101","day":1,"des":"在202年前的今天，1814年1月1日 (农历腊月初十)，太平天国农民起义领袖洪秀全诞辰。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/201104/8/22155856741.jpg","title":"太平天国农民起义领袖洪秀全诞辰","year":1814},{"_id":"18630101","day":1,"des":"在153年前的今天，1863年1月1日 (农历冬月十二)，奥林匹克运动创始人顾拜旦诞辰。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/2C1711494.jpg","title":"奥林匹克运动创始人顾拜旦诞辰","year":1863},{"_id":"18640101","day":1,"des":"在152年前的今天，1864年1月1日 (农历冬月廿二)，著名画家、篆刻家齐白石诞生。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200906/28/2155333769.jpg","title":"著名画家、篆刻家齐白石诞生","year":1864},{"_id":"19040101","day":1,"des":"在112年前的今天，1904年1月1日 (农历冬月十四)，京剧表演艺术家程砚秋诞辰。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/201001/3/23112010322185.jpg","title":"京剧表演艺术家程砚秋诞辰","year":1904},{"_id":"19120101","day":1,"des":"在104年前的今天，1912年1月1日 (农历冬月十三)，孙中山就任中华民国临时大总统。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/7517121366.jpg","title":"孙中山就任中华民国临时大总统","year":1912},{"_id":"19120101m1","day":1,"des":"在104年前的今天，1912年1月1日 (农历冬月十三)，中华书局创办。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200912/15/AB224321684.jpg","title":"中华书局创办","year":1912},{"_id":"19160101","day":1,"des":"在100年前的今天，1916年1月1日 (农历冬月廿六)，袁世凯复辟帝制。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200905/17/1D135547964.jpg","title":"袁世凯复辟帝制","year":1916},{"_id":"19170101","day":1,"des":"在99年前的今天，1917年1月1日 (农历腊月初八)，胡适发起文学改良运动。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/C417139836.jpg","title":"胡适发起文学改良运动","year":1917},{"_id":"19230101","day":1,"des":"在93年前的今天，1923年1月1日 (农历冬月十五)，彭湃担任海丰县农会会长。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/A117145730.jpg","title":"彭湃担任海丰县农会会长","year":1923},{"_id":"19260101","day":1,"des":"在90年前的今天，1926年1月1日 (农历冬月十七)，中国国民党第二次全国代表大会召开。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200905/17/52135318911.jpg","title":"中国国民党第二次全国代表大会召开","year":1926},{"_id":"19330101","day":1,"des":"在83年前的今天，1933年1月1日 (农历腊月初六)，中国驻檀香山领事馆落成。","lunar":"","month":1,"pic":"","title":"中国驻檀香山领事馆落成","year":1933},{"_id":"19340101","day":1,"des":"在82年前的今天，1934年1月1日 (农历冬月十六)，德国实施优生法。","lunar":"","month":1,"pic":"","title":"德国实施优生法","year":1934},{"_id":"19390101","day":1,"des":"在77年前的今天，1939年1月1日 (农历冬月十一)，国民党中常会决定开除汪精卫党籍。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/B617152129.jpg","title":"国民党中常会决定开除汪精卫党籍","year":1939},{"_id":"19420101","day":1,"des":"在74年前的今天，1942年1月1日 (农历冬月十五)，中苏美英等26国签署《联合国家共同宣言》。","lunar":"","month":1,"pic":"","title":"中苏美英等26国签署《联合国家共同宣言》","year":1942},{"_id":"19460101","day":1,"des":"在70年前的今天，1946年1月1日 (农历冬月廿八)，天皇非神宣言发表。","lunar":"","month":1,"pic":"","title":"天皇非神宣言发表","year":1946},{"_id":"19480101","day":1,"des":"在68年前的今天，1948年1月1日 (农历冬月廿一)，中国国民党革命委员会在香港成立。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/431725998.jpg","title":"中国国民党革命委员会在香港成立","year":1948},{"_id":"19490101","day":1,"des":"1949年1月1日 (农历腊月初三)，联合国促成克什米尔停火。","lunar":"","month":1,"pic":"","title":"联合国促成克什米尔停火","year":1949},{"_id":"19500101","day":1,"des":"1950年1月1日 (农历冬月十三)，巴黎统筹委员会成立。","lunar":"","month":1,"pic":"","title":"巴黎统筹委员会成立","year":1950},{"_id":"19560101m1","day":1,"des":"1956年1月1日 (农历冬月十九)，苏丹独立。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200912/15/2A2338291.jpg","title":"苏丹独立","year":1956},{"_id":"19570101","day":1,"des":"1957年1月1日 (农历腊月初一)，萨尔在第二次世界大战后重归德国。","lunar":"","month":1,"pic":"","title":"萨尔在第二次世界大战后重归德国","year":1957},{"_id":"19590101","day":1,"des":"1959年1月1日 (农历冬月廿二)，古巴革命胜利。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200905/17/00134053240.jpg","title":"古巴革命胜利","year":1959},{"_id":"19600101","day":1,"des":"1960年1月1日 (农历腊月初三)，刘家峡水利枢纽胜利截流。","lunar":"","month":1,"pic":"","title":"刘家峡水利枢纽胜利截流","year":1960},{"_id":"19600101m1","day":1,"des":"1960年1月1日 (农历腊月初三)，喀麦隆独立。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200912/15/86232818759.JPG","title":"喀麦隆独立","year":1960},{"_id":"19620101","day":1,"des":"1962年1月1日 (农历冬月廿五)，西萨摩亚独立。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200912/15/67232419442.jpg","title":"西萨摩亚独立","year":1962},{"_id":"19650101","day":1,"des":"1965年1月1日 (农历冬月廿九)，法塔赫领导的武装斗争开始。","lunar":"","month":1,"pic":"","title":"法塔赫领导的武装斗争开始","year":1965},{"_id":"19790101m1","day":1,"des":"1979年1月1日 (农历腊月初三)，中国与美国建立外交关系。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/B217238951.jpg","title":"中国与美国建立外交关系","year":1979},{"_id":"19790101","day":1,"des":"1979年1月1日 (农历腊月初三)，告台湾同胞书发表。","lunar":"","month":1,"pic":"","title":"告台湾同胞书发表","year":1979},{"_id":"19810101m1","day":1,"des":"1981年1月1日 (农历冬月廿六)，我国正式实行学位制度。","lunar":"","month":1,"pic":"","title":"我国正式实行学位制度","year":1981},{"_id":"19810101","day":1,"des":"1981年1月1日 (农历冬月廿六)，《中华人民共和国婚姻法》（修正草案）公布施行。","lunar":"","month":1,"pic":"","title":"《中华人民共和国婚姻法》（修正草案）公布施行","year":1981},{"_id":"19820101","day":1,"des":"1982年1月1日 (农历腊月初七)，全国农村工作会议纪要。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/5117254788.jpg","title":"全国农村工作会议纪要","year":1982},{"_id":"19840101","day":1,"des":"1984年1月1日 (农历冬月廿九)，中国工商银行成立。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200912/15/9E231726638.jpg","title":"中国工商银行成立","year":1984},{"_id":"19840101m2","day":1,"des":"1984年1月1日 (农历冬月廿九)，关于1984年农村工作的通知。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/F217315667.jpg","title":"关于1984年农村工作的通知","year":1984},{"_id":"19840101m1","day":1,"des":"1984年1月1日 (农历冬月廿九)，中国正式成为国际原子能机构成员国。","lunar":"","month":1,"pic":"","title":"中国正式成为国际原子能机构成员国","year":1984},{"_id":"19840101m3","day":1,"des":"1984年1月1日 (农历冬月廿九)，文莱独立。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200912/15/9E23234590.jpg","title":"文莱独立","year":1984},{"_id":"19850101","day":1,"des":"1985年1月1日 (农历冬月十一)，活跃农村经济，中共中央、国务院出台新政策。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/DA17325604.jpg","title":"活跃农村经济，中共中央、国务院出台新政策","year":1985},{"_id":"19860101","day":1,"des":"1986年1月1日 (农历冬月廿一)，里根与戈尔巴乔夫互致问候。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200905/17/74133316402.jpg","title":"里根与戈尔巴乔夫互致问候","year":1986},{"_id":"19880101","day":1,"des":"1988年1月1日 (农历冬月十二)，天安门城楼正式对外开放。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/1317333576.jpg","title":"天安门城楼正式对外开放","year":1988},{"_id":"19930101","day":1,"des":"1993年1月1日 (农历腊月初九)，捷克和斯洛伐克两共和国解体。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/201001/3/30225656536.jpg","title":"捷克和斯洛伐克两共和国解体","year":1993},{"_id":"19940101","day":1,"des":"1994年1月1日 (农历冬月二十)，欧洲经济区成立。","lunar":"","month":1,"pic":"","title":"欧洲经济区成立","year":1994},{"_id":"19950101","day":1,"des":"1995年1月1日 (农历腊月初一)，世界贸易组织成立。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/1C17349917.jpg","title":"世界贸易组织成立","year":1995},{"_id":"19980101","day":1,"des":"1998年1月1日 (农历腊月初三)，我国与南非建立外交关系。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/1017356651.jpg","title":"我国与南非建立外交关系","year":1998},{"_id":"19990101","day":1,"des":"1999年1月1日 (农历冬月十四)，欧元诞生。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200401/1/5D17427599.jpg","title":"欧元诞生","year":1999},{"_id":"20060101","day":1,"des":"2006年1月1日 (农历腊月初二)，中国政府废止农业税。","lunar":"","month":1,"pic":"","title":"中国政府废止农业税","year":2006},{"_id":"20100101","day":1,"des":"2010年1月1日 (农历冬月十七)，中国－东盟自贸区正式建成。","lunar":"","month":1,"pic":"http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/201001/2/171552010355549.jpg","title":"中国－东盟自贸区正式建成","year":2010}]
     */

    public int error_code;
    public String reason;
    /**
     * _id : 10850101
     * day : 1
     * des : 在931年前的今天，1085年1月1日 (农历腊月初三)，司马光主持编撰的《资治通鉴》成书。
     * lunar :
     * month : 1
     * pic : http://juheimg.oss-cn-hangzhou.aliyuncs.com/toh/200911/30/0F02714956.jpg
     * title : 司马光主持编撰的《资治通鉴》成书
     * year : 1085
     */

    public List<ResultBean> result;



    public static class ResultBean {
        private String _id;
        private int day;
        private String des;
        private String lunar;
        private int month;
        private String pic;
        private String title;
        private int year;

        @Override
        public String toString() {
            return "ResultBean{" +
                    "_id='" + _id + '\'' +
                    ", day=" + day +
                    ", des='" + des + '\'' +
                    ", lunar='" + lunar + '\'' +
                    ", month=" + month +
                    ", pic='" + pic + '\'' +
                    ", title='" + title + '\'' +
                    ", year=" + year +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "TodyHistory{" +
                "error_code=" + error_code +
                ", reason='" + reason + '\'' +
                ", result=" + result +
                '}';
    }
}
