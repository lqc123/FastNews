package com.lq.fastnews.model.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/5/5 0005.
 */
public class NewsInfo {

    /**
     * channelId : 5572a109b3cdc86cf39001db
     * channelName : 国内最新
     * desc : 截至5月4日，两市因重大事项处于停牌状态的地方国资上市公司已达76家，较2月底的37家呈快速上...
     * imageurls : []
     * link : http://kuaixun.stcn.com/2016/0505/12701948.shtml
     * nid : 17460941787312444066
     * pubDate : 2016-05-05 08:56:40
     * sentiment_display : 0
     * sentiment_tag : {"count":"15472","dim":"0","id":"8258","isbooked":0,"ishot":"0","name":"上市公司","type":"senti"}
     * source : 证券时报
     * title : 国企改革进入政策落地期 停牌上市公司已达76家
     */

    public String channelId;
    public String channelName;
    public String desc;
    public String link;
    public String nid;
    public String pubDate;
    public int sentiment_display;
    /**
     * count : 15472
     * dim : 0
     * id : 8258
     * isbooked : 0
     * ishot : 0
     * name : 上市公司
     * type : senti
     */

    public SentimentTagEntity sentiment_tag;
    public String source;
    public String title;
    public Object imageurls;

    public static class SentimentTagEntity {
        public String count;
        public String dim;
        public String id;
        public int isbooked;
        public String ishot;
        public String name;
        public String type;
    }
}
