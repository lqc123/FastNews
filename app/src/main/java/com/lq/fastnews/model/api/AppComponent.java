package com.lq.fastnews.model.api;
import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ApiServiceModule.class})
public interface AppComponent {
    ApiService getApiService();
}
