package com.lq.fastnews.model.bean.weather;

/**
 * Created by Administrator on 2016/5/22.
 */
public class Forecast {

    /**
     * date : 2016-05-23
     * fengli : 微风级
     * fengxiang : 无持续风向
     * hightemp : 23℃
     * lowtemp : 14℃
     * type : 阵雨
     * week : 星期一
     */

    public String date;
    public String fengli;
    public String fengxiang;
    public String hightemp;
    public String lowtemp;
    public String type;
    public String week;
}
