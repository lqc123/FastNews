package com.lq.fastnews.model.api;


public  class ApiDefine {

    static{
        System.loadLibrary("model_data");
    }
    public static native String api(int type);

//
//    public static final String API_HOST = "http://apis.baidu.com/";
//    public static final String API_KEY = api(0);
//    public static final String IMAGE = "http://image.baidu.com/data/imgs";
//
//    public static final String API_HOST_WEATHER = "http://op.juhe.cn/";
//    public static final String API_KEY_WEATHER = api(1);
//
//    public static final String API_HOST_BUS = "http://op.juhe.cn/";
//    public static final String API_KEY_BUS = api(2);
//
//    public static final String VIDEO="http://op.juhe.cn/onebox/movie/pmovie";
//    public static final  String VIDEO_KEY=api(3);
//
//    public static final String TODAY_HISTORY="http://api.juheapi.com/japi/toh";
//    public static final String TODAY_HISTORY_KEY=api(4);

    public static final String API_HOST = "http://apis.baidu.com/";
    public static final String API_KEY = "70a30f59c0c605a7837c7b211aa88f3c";
    public static final String IMAGE = "http://image.baidu.com/data/imgs";

//    请求地址：http://op.juhe.cn/onebox/weather/query
//    请求参数：cityname=%E5%90%88%E8%82%A5&dtype=json&key=53bebb554ce5ca0ba47f79f88833866a
//    请求方式：GET
    public static final String API_HOST_WEATHER = "http://op.juhe.cn/";
    public static final String API_KEY_WEATHER = "53bebb554ce5ca0ba47f79f88833866a";

//    请求地址：http://op.juhe.cn/189/bus/busline
//    请求参数：dtype=json&city=%E5%90%88%E8%82%A5&bus=902&key=299d18c4f75732b7d9b82f1868c01dfb
//    请求方式：GET
    public static final String API_HOST_BUS = "http://op.juhe.cn/";
    public static final String API_KEY_BUS = "299d18c4f75732b7d9b82f1868c01dfb";
    public static final String VIDEO="http://op.juhe.cn/onebox/movie/pmovie";
    public static final  String VIDEO_KEY="608c12182c5efe586b184f887451caff";
    public static final String TODAY_HISTORY="http://api.juheapi.com/japi/toh";
    public static final String TODAY_HISTORY_KEY="e7e495cd446fa33eb8be3405c715b1dd";

}
