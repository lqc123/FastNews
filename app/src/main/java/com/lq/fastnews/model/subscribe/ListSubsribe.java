package com.lq.fastnews.model.subscribe;

import com.lq.fastnews.iview.ListMvpView;
import com.lq.fastnews.utils.Const;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public abstract class ListSubsribe<T,V extends ListMvpView> extends BaseErrorSubsribe<T,V> {
    public ListSubsribe(V view) {
        super(view);
    }
    @Override
    public void onError(Throwable e) {
        super.onError(e);
//        mView.clearLoad();
    }

    @Override
    public void onCompleted() {
        super.onCompleted();
        mView.restore();
        mView.clearLoad();
    }

    @Override
    protected void reLoad() {
        mView.restore();
        mView.initHttp(Const.INIT);
    }
}
