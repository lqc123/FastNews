package com.lq.fastnews.model.bean.news;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class HeadImageInfo {
    public int height;
    public int width;
    public String url;

    @Override
    public String toString() {
        return "HeadImageInfo{" +
                "height=" + height +
                ", width=" + width +
                ", url='" + url + '\'' +
                '}';
    }
}
