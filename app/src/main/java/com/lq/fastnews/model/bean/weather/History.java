package com.lq.fastnews.model.bean.weather;

/**
 * Created by Administrator on 2016/5/22.
 */
public class History {

    /**
     * aqi : 42
     * date : 2016-05-15
     * fengli : 3-4级
     * fengxiang : 北风
     * hightemp : 25℃
     * lowtemp : 10℃
     * type : 晴
     * week : 星期天
     */

    public String aqi;
    public String date;
    public String fengli;
    public String fengxiang;
    public String hightemp;
    public String lowtemp;
    public String type;
    public String week;
}
