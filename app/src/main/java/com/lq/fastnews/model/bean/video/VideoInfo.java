package com.lq.fastnews.model.bean.video;

import com.google.gson.JsonObject;

import java.util.List;

/**
 * Created by Administrator on 2016/7/12.
 */
public class VideoInfo {
    public Object director;
    public String grade;
    public String gradeNum;

    public String iconaddress;
    /**
     * url地址
     */
    public String iconlinkUrl;
    /**
     * 时光网url
     */
    public String m_iconlinkUrl;

    public Object more;
    /**
     * 上映日期
     */
    public PlayDate playDate;
    public static class  PlayDate{
        public String date;
        /**
         * 日期
         */
        public String showname;
    }

    /**
     * 明星
     */
    public JsonObject star;

    public List<String> starName;

    public List<String> starLine;


    /**
     * 故事
     */
    public JsonObject story;
    /**
     * 今日50家影院上映
     */
    public String subHead;
    /**
     * 影名
     */
    public String tvTitle;
}
