package com.lq.fastnews.model.subscribe;

import android.util.Log;
import android.view.View;

import com.lq.fastnews.R;
import com.lq.fastnews.iview.LoadView;
import com.lq.fastnews.utils.LogUtil;
import com.lq.fastnews.utils.UIUtil;
import com.lq.fastnews.utils.netstatus.NetWorkUtil;

import org.jetbrains.annotations.NotNull;

import rx.Subscriber;

/**
 * 观察者
 */
public abstract class BaseSubsribe<T,V extends LoadView> extends Subscriber<T>  {
    private static final String TAG = "BaseSubsribe";
    protected V mView;

    public  BaseSubsribe( V view){
        mView = view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
    }

    @Override
    public void onNext(T t) {
//        Log.i(TAG, "response" + t.toString());
        if(t==null){
            UIUtil.showToast(R.string.data_load_error);
            return;
        }
        LogUtil.i(TAG,t.toString());
        if(mView==null)return;
        onSuccess(t);
    }

    @Override
    public void onCompleted() {
        Log.i(TAG, "onCompleted");
        if(mView==null){
            return;
        }
        mView.hideLoading();
    }

    public abstract void onSuccess(T result);

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        Log.i(TAG, "onError" + e.getMessage());
        if(NetWorkUtil.isNetAndShow()){
            UIUtil.showToast(e.getMessage());
        }
        if (mView==null){
            return;
        }
        mView.hideLoading();

    }


}
