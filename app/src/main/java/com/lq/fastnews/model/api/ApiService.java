package com.lq.fastnews.model.api;

import com.lq.fastnews.model.bean.Channel;
import com.lq.fastnews.model.bean.Result;
import com.lq.fastnews.model.bean.TodyHistory;
import com.lq.fastnews.model.bean.image.ImageResult;
import com.lq.fastnews.model.bean.weather.WeatherResult;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;


public interface ApiService {


    /**
     * 新闻列表
     * @param channelId
     * @param channelName
     * @param page
     * @return
     */
//    @Headers("apikey:"+ApiDefine.API_KEY)
    @GET("showapi_open_bus/channel_news/search_news")
    Observable<Result> queryNewsList(
            @Header("apikey") String apikey,
            @Query("channelId") String channelId,
            @Query("channelName") String channelName,
            @Query("page") int page
    );


    /**
     * 查询
     * @param channelId
     * @param channelName
     * @param title
     * @param page
     * @return
     */
//    @Headers("apikey:"+ApiDefine.API_KEY)
    @GET("showapi_open_bus/channel_news/search_news")
    Observable<Result> queryNews(
            @Header("apikey") String apikey,
            @Query("channelId") String channelId,
            @Query("channelName") String channelName,
            @Query("title") String title,
            @Query("page") int page
    );

    /**
     * 新闻频道
     * @return
     */
//    @Headers("apikey:"+ApiDefine.API_KEY)
    @GET("showapi_open_bus/channel_news/channel_news")
    Observable<Channel> queryChannel(
            @Header("apikey") String apikey
    );
    /**
     * 天气
     */
//    @Headers("apikey:"+ApiDefine.API_KEY)
    @GET("apistore/weatherservice/recentweathers")
    Observable<WeatherResult> queryWeather(
            @Header("apikey") String apikey,
            @Query("cityname")String cityname
    );
    int RN=20;
    int FROM=1;
    String TAG="全部";
    //    ?col=动漫&tag=全部&pn=40&rn=20&from=1
    @GET(ApiDefine.IMAGE)
    Observable<ImageResult> queryImageList(
            @Query("col") String col,
            @Query("tag") String tag,
            @Query("pn") int pn,
            @Query("from") int from,
            @Query("rn") int rn

    );

    /**
     * 最近影讯
     */
    @FormUrlEncoded
    @POST(ApiDefine.VIDEO)
    Observable queryVideo(
            @Field("key") String key,
            @Field("dtype") String dtype,
            @Field("city") String city
    );
    @FormUrlEncoded
    @POST(ApiDefine.TODAY_HISTORY)
    Observable<TodyHistory> queryTodayHistory(
            @Field("key") String key,
            @Field("v") String v,
            @Field("month") String month,
            @Field("day") String day
    );

//    @GET("showapi_open_bus/channel_news/channel_news")
//    Observable<Channel> queryWeather(
//            @Header("apikey") String key
//    );

//
//    .out: http://image.baidu.com/data/imgs?col=%E5%8A%A8%E6%BC%AB&tag=%E5%85%A8%E9%83%A8&pn=20&rn=20&from=1
//  http://image.baidu.com/data/imgs?col=%E5%8A%A8%E6%BC%AB&tag=%E5%85%A8%E9%83%A8&pn=40&rn=20&from=1
//     http://image.baidu.com/data/imgs?col=%E5%8A%A8%E6%BC%AB&tag=%E5%85%A8%E9%83%A8&pn=60&rn=20&from=1

//
//    @GET("apistore/weatherservice/cityname")
//    Observable<WeatherResultBean> queryWeather(@Header("apikey") String apikey, @Query("cityname") String cityname);
//
//    @GET("/v1/topic/{id}")
//    void getTopic(
//            @Path("id") String id,
//            @Query("mdrender") Boolean mdrender,
//            Callback<Result<TopicWithReply>> callback
//    );
//
//    @FormUrlEncoded
//    @POST("/v1/topics")
//    void newTopic(
//            @Field("accesstoken") String accessToken,
//            @Field("tab") TabType tab,
//            @Field("title") String title,
//            @Field("content") String content,
//            Callback<Void> callback
//    );
//
//    @FormUrlEncoded
//    @POST("/v1/topic/{topicId}/replies")
//    void replyTopic(
//            @Field("accesstoken") String accessToken,
//            @Path("topicId") String topicId,
//            @Field("content") String content,
//            @Field("reply_id") String replyId,
//            Callback<Map<String, String>> callback
//    );
//
//    @FormUrlEncoded
//    @POST("/v1/reply/{replyId}/ups")
//    void upTopic(
//            @Field("accesstoken") String accessToken,
//            @Path("replyId") String replyId,
//            Callback<TopicUpInfo> callback
//    );
//
//    //=====
//    // 用户
//    //=====
//
//    @GET("/v1/user/{loginName}")
//    void getUser(
//            @Path("loginName") String loginName,
//            Callback<Result<User>> callback
//    );
//
//    @FormUrlEncoded
//    @POST("/v1/accesstoken")
//    void accessToken(
//            @Field("accesstoken") String accessToken,
//            Callback<LoginInfo> callback
//    );
//
//    //=========
//    // 消息通知
//    //=========
//
//    @GET("/v1/message/count")
//    void getMessageCount(
//            @Query("accesstoken") String accessToken,
//            Callback<Result<Integer>> callback
//    );
//
//    @GET("/v1/messages")
//    void getMessages(
//            @Query("accesstoken") String accessToken,
//            @Query("mdrender") Boolean mdrender,
//            Callback<Result<Notification>> callback
//    );
//
//    @FormUrlEncoded
//    @POST("/v1/message/mark_all")
//    void markAllMessageRead(
//            @Field("accesstoken") String accessToken,
//            Callback<Void> callback
//    );

}
