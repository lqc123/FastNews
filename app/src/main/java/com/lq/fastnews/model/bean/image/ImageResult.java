package com.lq.fastnews.model.bean.image;

import com.lq.fastnews.model.bean.image.ImageInfo;

import java.util.List;

/**
 * Created by Administrator on 2016/5/10 0010.
 */
public class ImageResult {



    public String col;
    public int returnNumber;
    public String sort;
    public int startIndex;
    public String tag;
    public String tag3;
    public int totalNum;
    public List<ImageInfo> imgs;

    @Override
    public String toString() {
        return "ImageResult{" +
                "col='" + col + '\'' +
                ", returnNumber=" + returnNumber +
                ", sort='" + sort + '\'' +
                ", startIndex=" + startIndex +
                ", tag='" + tag + '\'' +
                ", tag3='" + tag3 + '\'' +
                ", totalNum=" + totalNum +
                ", imgs=" + imgs +
                '}';
    }
}
