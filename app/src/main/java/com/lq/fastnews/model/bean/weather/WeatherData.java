package com.lq.fastnews.model.bean.weather;

import java.util.List;

/**
 * Created by Administrator on 2016/5/22.
 */
public class WeatherData {
    public String city;
    public String cityid;
    public List<Forecast> forecast;
    public List<History> history;
    public Today today;
}
