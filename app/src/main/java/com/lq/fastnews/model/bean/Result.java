package com.lq.fastnews.model.bean;

import com.google.gson.JsonObject;

/**
 * Created by Administrator on 2016/5/5 0005.
 */
public class Result {

    public JsonObject showapi_res_body;
    public int showapi_res_code;
    public String showapi_res_error;

    @Override
    public String toString() {
        return "Result{" +
                "showapi_res_body=" + showapi_res_body +
                ", showapi_res_code=" + showapi_res_code +
                ", showapi_res_error='" + showapi_res_error + '\'' +
                '}';
    }
}
