
package com.lq.fastnews.iview;


import android.view.View;

public interface LoadView extends BaseView{
    int ERROR=0;
    int NET_ERROR=1;
    int EMPTY=2;

    void showLoading();

    void hideLoading();

    void showError(String msg, View.OnClickListener onClickListener,int type);
    void restore();

}
