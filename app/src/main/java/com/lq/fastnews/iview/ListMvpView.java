package com.lq.fastnews.iview;



import java.util.List;

/**
 * Created by Administrator on 2016/5/6 0006.
 */
public interface ListMvpView<T>  extends LoadView {
    void refresh(List<T> data);
    void loadMore(List<T> data);
    void initHttp(int type);
    void clearLoad();
}
