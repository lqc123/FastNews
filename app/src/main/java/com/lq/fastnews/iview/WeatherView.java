package com.lq.fastnews.iview;


import com.lq.fastnews.model.bean.weather.WeatherResult;

/**
 * Created by Administrator on 2016/5/22.
 */
public interface WeatherView extends LoadView {
    void loadData(WeatherResult result);
}
