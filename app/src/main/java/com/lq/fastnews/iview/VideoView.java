package com.lq.fastnews.iview;

import com.lq.fastnews.model.bean.video.VideoInfo;

import java.util.List;

/**
 * Created by Administrator on 2016/7/12.
 */
public interface VideoView extends LoadView {
    void loadVideo(List<VideoInfo> datas);
}
