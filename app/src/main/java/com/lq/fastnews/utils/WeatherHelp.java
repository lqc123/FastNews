package com.lq.fastnews.utils;


import com.lq.fastnews.R;

import java.util.HashMap;

/**
 * Created by Administrator on 2016/5/26 0026.
 */
public class WeatherHelp {
    public static HashMap<String, Integer> getWeatherType(){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("晴", R.mipmap.w0);
        map.put("多云", R.mipmap.w1);
        map.put("阴", R.mipmap.w2);
        map.put("阵雨", R.mipmap.w3);

        map.put("雷雨", R.mipmap.w4);
        map.put("雷阵雨伴有冰雹", R.mipmap.w5);
        map.put("雨夹雪", R.mipmap.w6);
        map.put("小雨", R.mipmap.w7);

        map.put("中雨", R.mipmap.w8);
        map.put("大雨", R.mipmap.w9);
        map.put("暴雨", R.mipmap.w10);
        map.put("大暴雨", R.mipmap.w11);

        map.put("特大暴雨", R.mipmap.w12);
        map.put("阵雪", R.mipmap.w13);
        map.put("小雪", R.mipmap.w14);
        map.put("中雪", R.mipmap.w15);

        map.put("大雪", R.mipmap.w16);
        map.put("暴雪", R.mipmap.w17);
        map.put("雾", R.mipmap.w18);
        map.put("冻雨", R.mipmap.w19);

        map.put("沙尘暴", R.mipmap.w20);
        map.put("小到中雨", R.mipmap.w21);
        map.put("中到大雨", R.mipmap.w22);
        map.put("大到暴雨", R.mipmap.w23);

        map.put("暴到大暴雨", R.mipmap.w24);
        map.put("大暴到特大暴雨", R.mipmap.w25);
        map.put("小到中雪", R.mipmap.w26);
        map.put("中到大雪", R.mipmap.w27);

        map.put("大到暴雪", R.mipmap.w28);
        map.put("浮尘", R.mipmap.w29);
        map.put("扬沙", R.mipmap.w30);
        map.put("强沙尘暴", R.mipmap.w31);
        return map;
    }
}
