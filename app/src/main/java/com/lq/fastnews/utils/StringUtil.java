package com.lq.fastnews.utils;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;


import com.lq.fastnews.MyApp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/3/1.
 */
public class StringUtil {


    @SuppressLint("SimpleDateFormat")
    public static String longtimeToDate(long time) {
        String dateStr = null;
        try {
            Date now = new Date(time * 1000);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");// 可以方便地修改日期格式
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
            dateStr = dateFormat.format(now);
        } catch (Exception e) {
            e.printStackTrace();
            dateStr="0000-00-00 00:00:00";
        }
        return dateStr;
    }

    /**
     * 修改时间戳转化时间
     * by黄海杰 at:2015年7月27日 15:44:16
     *
     * @param time
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String longtimeToDayDate(long time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm");// 可以方便地修改日期格式
        String dateStr = dateFormat.format(new Date(time));
        return dateStr;
    }

    public static String getCurrentTime(String format) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        String currentTime = sdf.format(date);
        return currentTime;
    }


    @SuppressLint("SimpleDateFormat")
    public static String longtimeToDateYMD(long time) {
        Date now = new Date(time);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");// 可以方便地修改日期格式
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        String dateStr = dateFormat.format(now);
        return dateStr;
    }


    /**
     * 根据yyyy-MM-dd HH:mm:ss格式时间字符串转为long型时间戳
     *
     * @param dateStr
     * @return date long
     * by:Hankkin at:2015年6月25日 17:38:25 修改时区设置
     */
    public static long stringDateToLong(String dateStr) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if(date==null){
            return 0;
        }
        return date.getTime();
    }

    public static String getCurrentTime() {
        return getCurrentTime("yyyy-MM-dd  HH:mm:ss");
    }


    /**
     * 将时间戳转为代表"距现在多久之前"的字符串
     * 修改为向下取整
     *
     * @param dateTime 时间戳
     * @return
     */
    public static String getStandardDate(long dateTime) {

        StringBuffer sb = new StringBuffer();
//		long t = Long.parseLong(timeStr);
//		long time = System.currentTimeMillis() - (t*1000);
        long time = System.currentTimeMillis() - (dateTime);
        long mill = (long) Math.floor(time / 1000);//秒前

        long minute = (long) Math.floor(time / 60 / 1000.0f);// 分钟前

        long hour = (long) Math.floor(time / 60 / 60 / 1000.0f);// 小时

        long day = (long) Math.floor(time / 24 / 60 / 60 / 1000.0f);// 天前

        if (day - 1 > 0) {
            sb.append(day + "天");
        } else if (hour - 1 > 0) {
            if (hour >= 24) {
                sb.append("1天");
            } else {
                sb.append(hour + "小时");
            }
        } else if (minute - 1 > 0) {
            if (minute == 60) {
                sb.append("1小时");
            } else {
                sb.append(minute + "分钟");
            }
        } else if (mill - 1 > 0) {
            if (mill == 60) {
                sb.append("1分钟");
            } else {
                sb.append(mill + "秒");
            }
        } else {
            sb.append("刚刚");
        }
        if (!sb.toString().equals("刚刚")) {
            sb.append("前");
        }
        return sb.toString();
    }

    /**
     * 根据时间戳的差获取时间差
     * 修改超过一天的显示时间
     *
     * @param dateTime
     * @return
     */
    public static String getDateAgo(long dateTime) {

        String days = null;
//		long t = Long.parseLong(timeStr);
//		long time = System.currentTimeMillis() - (t*1000);
        long timeInterval = (System.currentTimeMillis() - (dateTime)) / 1000;


        if (timeInterval < 60) {
            days = "1分钟前";
        } else if (timeInterval < 3600) {
            days = "" + (int) Math.round(timeInterval / 60) + "分钟内";
        } else if (timeInterval < 86400) {
            if (timeInterval % 3600 > 1800) {
                days = "" + (int) Math.round((timeInterval / 3600) + 1) + "小时内";
            } else {
                days = "" + (int) Math.round((timeInterval / 3600)) + "小时内";
            }
        }
//		else if (timeInterval<2592000){
//			days = ""+(int)Math.floor(timeInterval/86400)+"天前";
//		}
//		else if (timeInterval<31536000){
//			days = ""+(int)Math.floor(timeInterval/2592000)+"个月前";
//		}
//		else {
//			days = ""+(int)Math.floor(timeInterval/31536000)+"年前";
//		}
        else {
            days = longtimeToDayDate(dateTime);
        }
        return days;
    }

    /**
     * 实现文本复制功能 add by lif
     *
     * @param content
     */
    public static void copy(String content) {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager) MyApp.mContext
                .getSystemService(Context.CLIPBOARD_SERVICE);
        cmb.setText(content.trim());

    }

    /**
     * 实现粘贴功能 add by lif
     *
     * @return
     */
    public static String paste() {
        // 得到剪贴板管理器
        ClipboardManager cmb = (ClipboardManager)MyApp.mContext
                .getSystemService(Context.CLIPBOARD_SERVICE);
        return cmb.getText().toString().trim();
    }



    /**
     * 验证是否手机号码
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobileNO(String mobiles) {
        String telRegex = "[1]\\d{10}";//"[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。
        if (TextUtils.isEmpty(mobiles)) return false;
        else return mobiles.matches(telRegex);
    }

    /**
     * 中文识别
     */
    public static boolean hasChinese(String source) {
        String reg_charset = "([\\u4E00-\\u9FA5]*+)";
        Pattern p = Pattern.compile(reg_charset);
        Matcher m = p.matcher(source);
        boolean hasChinese = false;
        while (m.find()) {
            if (!"".equals(m.group(1))) {
                hasChinese = true;
            }
        }
        return hasChinese;
    }

    /**
     * 用户名规则判断
     *
     * @param uname
     * @return
     */
    public static boolean isAccountStandard(String uname) {
        Pattern p = Pattern.compile("[A-Za-z0-9_]+");
        Matcher m = p.matcher(uname);
        return m.matches();
    }

    // java 合并两个byte数组
    public static byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
        byte[] byte_3 = new byte[byte_1.length + byte_2.length];
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
        return byte_3;
    }

    /**
     * 获取手机号
     * by Hankkin
     * @param context
     * @return
     */
    public static String getPhoneNumber(Context context){
        TelephonyManager mTelephonyMgr;
        mTelephonyMgr = (TelephonyManager)  context.getSystemService(Context.TELEPHONY_SERVICE);
        return mTelephonyMgr.getLine1Number();
    }
public static String getResString(int id){
    return MyApp.mContext.getResources().getString(id);
}

}
