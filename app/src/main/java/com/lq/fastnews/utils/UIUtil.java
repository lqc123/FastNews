package com.lq.fastnews.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.R;
import com.lq.fastnews.ui.activity.MainActivity;
import com.lq.fastnews.ui.activity.WeatherActivity;


/**
 * Created by 95 on 2016/1/18.
 */
public class UIUtil {
    public static Snackbar getSnackbar(String message, View view) {
        Snackbar snackbar = Snackbar.make(view, message + "", Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(Color.parseColor("#666666"));
        setSnackbarMessageTextColor(snackbar, Color.parseColor("#FFFFFF"));
//		snackbar.setActionTextColor(context.getResources().getColor(R.color.color_white));
        return snackbar;
    }

    private static void setSnackbarMessageTextColor(Snackbar snackbar, int color) {
        View view = snackbar.getView();
        TextView snackbar_text = (TextView) view.findViewById(R.id.snackbar_text);
        snackbar_text.setTextColor(color);
        snackbar_text.setGravity(Gravity.CENTER);
    }

    public static void showSnackbar(String message, View view) {
        if (view == null) {

            return;
        }
        Snackbar snackbar = getSnackbar(message, view);
        snackbar.show();
//		snackbar.setActionTextColor(context.getResources().getColor(R.color.color_white));
    }

    public static void showSnackbar(int resid, View view) {
        if (view == null) {
            return;
        }
        Snackbar snackbar = Snackbar.make(view, resid, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundColor(Color.parseColor("#666666"));
        setSnackbarMessageTextColor(snackbar, Color.parseColor("#FFFFFF"));
        snackbar.show();
//		snackbar.setActionTextColor(context.getResources().getColor(R.color.color_white));
    }

    /**
     * @param context
     * 上下文环境
     * @param message
     * toast 的信息
     */
    private static Toast mToast = null;

    public static final void showToast(String message) {
        if (null == mToast) {
            mToast = Toast.makeText(MyApp.mContext, message, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(message);
        }
        mToast.show();

    }

    public static final void showToast(int message) {

        if (null == mToast) {
            mToast = Toast.makeText(MyApp.mContext, message, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(message);
        }
        mToast.show();

    }

    private static long lastClickTime;

    public synchronized static final boolean isFastClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }


    public static Resources getRes() {
        return MyApp.mContext.getResources();
    }
    public static int getColor(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return getRes().getColor(id,null);
        }else {
            return getRes().getColor(id);

        }
    }



    public static final void sout(String objects) {
        System.out.println(objects + "");
    }


    public static void startActivity(Context c,Class clazz){
        c.startActivity(new Intent(c,clazz));
    }
}
