package com.lq.fastnews.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.lq.fastnews.MyApp;
import com.lq.fastnews.R;

/**
 * Created by Administrator on 2016/8/18 0018.
 */
public class ImageLoadUtil {
    public static void clearMemory() {
        Glide.get(MyApp.mContext).clearMemory();
    }

    /**
     *列表专用
     * @param context
     * @param url
     * @param imageView
     */
    public static void displayImageList(Context context,String url, ImageView imageView){
        if(!TextUtils.isEmpty(url)){
            Glide.with(context).load(url).fitCenter().placeholder(R.mipmap.bg).error(R.mipmap.bg).into(imageView);
        }else {
            imageView.setImageResource(R.mipmap.bg);
        }

    }
    public static void displayImage(RequestManager manager, String url, ImageView imageView) {
        if(!TextUtils.isEmpty(url)){
            manager.load(url).fitCenter().placeholder(R.mipmap.bg).error(R.mipmap.bg).into(imageView);
        }else {
            imageView.setImageResource(R.mipmap.bg);
        }
    }
    public static void loadNewsHead(Activity activity, String url, ImageView imageView){
        Glide.with(activity).load(url).fitCenter().into(imageView);
    }
    public static void clear(View view){
        Glide.clear(view);
    }
}
