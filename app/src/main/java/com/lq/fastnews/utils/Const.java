package com.lq.fastnews.utils;

/**
 * Created by Administrator on 2016/5/10 0010.
 */
public interface Const {
    String TYPE="TYPE";
    String CHANNEL = "CHANNEL";

    String IMAGE_TITLE="IMAGE_TITLE";
    String[] imageTitles={"美女","动漫","明星","汽车","摄影","美食"};

    int INIT=1;
    int LOAD=2;

    String TITLE="TITLE";
    String URL="URL";
    String URL_IMAGE="URL_IMAGE";
    int TIME=500;
    int LONG_TIME=1000;
    int LLONG_TIME=2000;
    String TEXT="TEXT";
}
