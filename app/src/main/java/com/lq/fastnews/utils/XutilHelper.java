package com.lq.fastnews.utils;

import android.os.Handler;
import android.os.Looper;


/**
 * Created by Administrator on 2016/5/8.
 */
public class XutilHelper {

    private static Handler mHandler;

    public static void post(Runnable run){
        getHandler().post(run);
    }

    private static Handler getHandler() {
        if(mHandler==null){
            synchronized (XutilHelper.class){
                if(mHandler==null){
                    mHandler = new Handler(Looper.getMainLooper());
                }
            }
        }
        return mHandler;
    }

    public static void postDelayed(Runnable run,int time){
        getHandler().postDelayed(run,time);
    }

    public static void removeCallbacks(Runnable run){
       getHandler().removeCallbacks(run);
    }

    public static void removeAllCallbacks(Object o){
      getHandler().removeCallbacksAndMessages(o);
    }



}
