package com.lq.fastnews.utils;


import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.R;
import com.lq.fastnews.ui.view.recycler.OnRecyclerPauseListen;


import java.lang.ref.WeakReference;


public class MyViewUtils {
	public static void removeView(View view){
		if (view != null) {
			WeakReference<View> w = new WeakReference<>(view);
			View v = w.get();
			if(v==null) return;
			ViewParent parent = v.getParent();
			if (parent != null && parent instanceof ViewGroup) {
				ViewGroup group = (ViewGroup) parent;
				group.removeView(v);
			}
		}
	}
	public static void setRefreshView(SwipeRefreshLayout refresh){
		WeakReference<SwipeRefreshLayout> w = new WeakReference<>(refresh);
		SwipeRefreshLayout s = w.get();
		if(s==null){
			return;
		}
		s.setColorSchemeResources(
				android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);
		s. setProgressBackgroundColorSchemeResource(R.color.refresh_bg);
	}
	public static void setRecyclerView(RecyclerView recycler,boolean isStarLayoutAnim,boolean isLoadImage){
		//setHasFixedSize()方法用来使RecyclerView保持固定的大小，该信息被用于自身的优化。


	}

	public static void setRecyclerView(RecyclerView recycler, boolean isStarLayoutAnim, boolean isLoadImage, RecyclerView.LayoutManager layoutManager){
		WeakReference<RecyclerView> w = new WeakReference<>(recycler);
		RecyclerView r = w.get();
		if (r==null) return;
		recycler.setHasFixedSize(true);
		recycler.setLayoutManager(layoutManager);
		if(isStarLayoutAnim){
			TranslateAnimation animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_SELF,
					0f, Animation.RELATIVE_TO_SELF, 6f, Animation.RELATIVE_TO_SELF, 0);
			animation.setInterpolator(new DecelerateInterpolator());
			animation.setDuration(350);
			animation.setStartOffset(150);
			LayoutAnimationController lac = new LayoutAnimationController(animation, 0.12f);
			lac.setInterpolator(new DecelerateInterpolator());
			r.setLayoutAnimation(lac);
		}
		if(isLoadImage){
				r.addOnScrollListener(new OnRecyclerPauseListen(true, true));
		}
	}

	public static void setWebView(WebView webView){
		WeakReference<WebView> wr = new WeakReference<>(webView);
		WebView w = wr.get();
		if(w==null) return;
		WebSettings settings = w.getSettings();
		settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
		settings.setJavaScriptEnabled(true);
		w.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		settings.setDefaultTextEncodingName("UTF-8");
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setBuiltInZoomControls(false);
		settings.setSupportMultipleWindows(true);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setSupportZoom(false);
		settings.setDomStorageEnabled(true);
		settings.setLoadsImagesAutomatically(true);

	}


}
