package com.lq.fastnews.utils.netstatus;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.R;
import com.lq.fastnews.utils.StringUtil;
import com.lq.fastnews.utils.UIUtil;

/**
 * @Title NetWorkUtil
 * @Package com.ta.util.netstate
 * @Description 是检测网络的一个工具包
 */
public class NetWorkUtil {
//   获取当前的网络状态 -1：没有网络 1：WIFI网络2：wap 网络3：net网络
    public static final int wifi=1;
    public static final int CMWAP=2;
    public static final int CMNET=3;
    public static final int noneNet=-1;

    /**
     * 网络是否可用
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager mgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = mgr.getAllNetworkInfo();
        if (info != null) {
            for (int i = 0; i < info.length; i++) {
                if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否联网和是否可用，以及显示信息
     *
     * @return
     */
    public static boolean isNetAndShow() {
        if (!isNetworkConnected()) {
            UIUtil.showToast(StringUtil.getResString(R.string.line_net));
            return false;
        }
        if (!isNetworkAvailable(MyApp.mContext)) {
            UIUtil.showToast(StringUtil.getResString(R.string.net_no_available));
            return false;
        }
        return true;
    }

    /**
     * 判断是否有网络连接
     *
     * @return
     */
    public static boolean isNetworkConnected() {

        {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) MyApp.mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager
                    .getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    /**
     * 判断WIFI网络是否可用
     *
     * @return
     */
    public static boolean isWifiConnected() {

            ConnectivityManager mConnectivityManager = (ConnectivityManager) MyApp.mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWiFiNetworkInfo = mConnectivityManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWiFiNetworkInfo != null) {
                return mWiFiNetworkInfo.isAvailable();
            }

        return false;
    }

    /**
     * 判断MOBILE网络是否可用
     *
     * @return
     */

    public static boolean isMobileConnected() {

        ConnectivityManager mConnectivityManager = (ConnectivityManager)MyApp.mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo mMobileNetworkInfo = mConnectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mMobileNetworkInfo != null) {
            return mMobileNetworkInfo.isAvailable();
        }

        return false;
    }

    /**
     * 获取当前网络连接的类型信息
     *
     * @param context
     * @return
     */
    public static int getConnectedType(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager
                    .getActiveNetworkInfo();
            if (mNetworkInfo != null && mNetworkInfo.isAvailable()) {
                return mNetworkInfo.getType();
            }
        }
        return -1;
    }

    /**
     * @return
     * @author 白猫
     * <p>
     * 获取当前的网络状态 -1：没有网络 1：WIFI网络2：wap 网络3：net网络
     */
    public static int getAPNType() {
        ConnectivityManager connMgr = (ConnectivityManager) MyApp.mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo == null) {
            return noneNet;
        }
        int nType = networkInfo.getType();

        if (nType == ConnectivityManager.TYPE_MOBILE) {
            if (networkInfo.getExtraInfo().toLowerCase().equals("cmnet")) {
                return CMNET;
            } else {
                return CMWAP;
            }
        } else if (nType == ConnectivityManager.TYPE_WIFI) {
            return wifi;
        }
        return noneNet;

    }

    public static boolean isWifiOpen() {
        boolean isWifiConnect = false;
        ConnectivityManager cm = (ConnectivityManager)MyApp.mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        // check the networkInfos numbers
        NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
        for (int i = 0; i < networkInfos.length; i++) {
            if (networkInfos[i].getState() == NetworkInfo.State.CONNECTED) {
                if (networkInfos[i].getType() == ConnectivityManager.TYPE_MOBILE) {
                    isWifiConnect = false;
                }
                if (networkInfos[i].getType() == ConnectivityManager.TYPE_WIFI) {
                    isWifiConnect = true;
                }
            }
        }
        return isWifiConnect;
    }


}
