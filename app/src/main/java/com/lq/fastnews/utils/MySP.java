package com.lq.fastnews.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.lq.fastnews.MyApp;


public class MySP {
    public static final String CHECK_PHONE="check_phone";
    public static final String MEMBER_ID ="member_id";
    private static final String PREFERENCE_NAME = "config";
    public static final String USER_ID = "user_id";
    public static final String TOKEN = "token";
 public static final String NAME = "name";
public static final String STORE_ID="store_id";
    public static final String IS_MOBILE="IS_MOBILE";

    private static SharedPreferences mSP = null;

    public static final String CITY="CITY";
    public static boolean saveData( String key, Object value) {
        if ( key == null || value == null) {
            return false;
        }
        createSp();
        SharedPreferences.Editor editor = mSP.edit();

        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else {
            return false;
        }
        return editor.commit();
    }

    public static String getStringData(String key) {

        createSp();

        return mSP.getString(key, "");

    }


    public static String getCity(){
        createSp();
        return mSP.getString(CITY,"合肥");
    }
    public static boolean getBooleanData(String key) {

        createSp();
        return mSP.getBoolean(key, false);

    }

    public static int getIntData(String key) {
        int value = 0;
        if (key == null) {
            return value;
        }
        createSp();
        value = mSP.getInt(key, 0);
        return value;

    }

    public static long getLongData( String key) {
        long value = -1;
        if ( key == null) {
            return value;
        }
        createSp();
        value = mSP.getLong(key, 0);
        return value;

    }

    public static String getID() {
        createSp();
        return mSP.getString(USER_ID, "");
    }

    public static String getStoreID() {
        createSp();
        return mSP.getString(STORE_ID, "");
    }
  public static String getName() {
        createSp();
        return mSP.getString(NAME, "");
    }

    public static String getToken() {
        createSp();
        return mSP.getString(TOKEN, "");
    }

    public static boolean deleteKey( String key) {
        if ( key == null) {
            return false;
        }
        createSp();
        SharedPreferences.Editor editor = mSP.edit();
        editor.remove(key);
        return editor.commit();
    }

    public static boolean deleteKeys( String... keys) {
        if ( keys == null) {
            return false;
        }
        createSp();
        SharedPreferences.Editor editor = mSP.edit();
        for (int i = 0; i < keys.length; i++) {
            editor.remove(keys[i]);
        }
        return editor.commit();
    }

    private static void createSp() {
        if (mSP == null) {
            mSP = MyApp.mContext.getSharedPreferences(PREFERENCE_NAME,
                    Context.MODE_PRIVATE);

        }
    }
    public static boolean isMobileLoadImage(){
        createSp();
        return mSP.getBoolean(IS_MOBILE, true);
    }
    public static void clear(){
        createSp();
        mSP.edit().clear().commit();
    }

}
