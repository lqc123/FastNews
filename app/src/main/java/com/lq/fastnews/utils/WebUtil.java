package com.lq.fastnews.utils;

import android.text.TextUtils;
import android.widget.TextView;

/**
 * Created by 蔡小木 on 2016/3/7 0007.
 */
public class WebUtil {
    public static final String BASE_URL = "file:///android_asset/";
    public static final String MIME_TYPE = "text/html";
    public static final String ENCODING = "utf-8";
//    public static final String FAIL_URL = "http//:daily.zhihu.com/";

    private static final String CSS_LINK_PATTERN = " <link href=\"%s\" type=\"text/css\" rel=\"stylesheet\" />";
    private static final String NIGHT_DIV_TAG_START = "<div class=\"night\">";
    private static final String NIGHT_DIV_TAG_END = "</div>";
    private static final String IMAGE_TAG_START = "<p class=\"img-caption\"><img alt=\"\" ";
    private static final String IMAGE_TAG_END ="  width=100%/>";
    private static final String HEAD =" <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"zh-cn\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/><style type=\"text/css\"> body{\n" +
            "    font-family:arial,sans-serif;\n" +
            "\tpadding: 0 15px;\n" +
            "\tcolor: #666;\n" +
            "\tbackground: #f8f8f8;\n" +
            "}</style></head><body>" ;
    private static final String END="</body></html>";
    private static final String BODY="<body>";
    private static final String P_TAG_START ="<p>";
    private static final String P_TAG_END ="</p>";
    private static final String H_TAG_START="<h3 style=\"text-align:center;\">";
    private static final String H_TAG_END="</h3>";

    private static final String DIV_IMAGE_PLACE_HOLDER = "class=\"img-place-holder\"";
    private static final String DIV_IMAGE_PLACE_HOLDER_IGNORED = "class=\"img-place-holder-ignored\"";

    public static String BuildHtmlWithCss(String html,boolean isNightMode) {
        StringBuilder result = new StringBuilder();
        result.append(HEAD);
//
//        for (String cssUrl : cssUrls) {
//            result.append(String.format(CSS_LINK_PATTERN, cssUrl));
//        }
        if (isNightMode) {
            result.append(NIGHT_DIV_TAG_START);
        }
        result.append(html);
        if (isNightMode) {
            result.append(NIGHT_DIV_TAG_END);
        }
        result.append(END);
        return result.toString();
    }

    public static String BuildHtmlWithCss(String html,String image){
        StringBuffer buffer=new StringBuffer();
        if(TextUtils.isEmpty(html)){
            return "";
        }
        String html1 = html.replace("[", "").replace("]", "");
        if(TextUtils.isEmpty(html1)){
            return "";
        }
        String[] split = html1.replace("\"","").split(",");
        int length = split.length;
        for (int i=0;i<length-1;i++){
            String s = split[i];
            if(s.contains("height")||s.contains("width")){

            }else {
//                System.out.println("s"+s);
                if(s.contains("url")){
                    if(!s.contains(image))
                    buffer.append(IMAGE_TAG_START+s.replace("url","src")+IMAGE_TAG_END+P_TAG_END);
                }else {
                    buffer.append(P_TAG_START+s+P_TAG_END);
//                    if(i==0){
//                        buffer.append(H_TAG_START+s+H_TAG_END);
//                    }else {
//                        buffer.append(P_TAG_START+s+P_TAG_END);
//                    }
                }

            }

        }
        return BuildHtmlWithCss(buffer.toString(),false);
    }
}
