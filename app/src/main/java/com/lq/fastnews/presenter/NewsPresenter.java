package com.lq.fastnews.presenter;

import com.lq.fastnews.iview.LoadView;
import com.lq.fastnews.iview.NetMvpView;
import com.lq.fastnews.iview.NewsView;
import com.lq.fastnews.model.api.ApiDefine;
import com.lq.fastnews.model.subscribe.BaseErrorSubsribe;
import com.lq.fastnews.model.subscribe.BaseSubsribe;
import com.lq.fastnews.model.bean.Channel;
import com.lq.fastnews.utils.Const;

import java.util.List;

/**
 * Created by Administrator on 2016/5/6 0006.
 */
public class NewsPresenter extends LoadPresenter<NewsView<List<Channel.ShowapiResBodyEntity.ChannelListEntity>>> {


    public void newsTitle() {

        addObservable(getApiService().queryChannel(ApiDefine.API_KEY),new BaseErrorSubsribe<Channel,NewsView>(mMvpView) {
            @Override
            protected void reLoad() {
                mView.restore();
                mView.reload();
            }

            @Override
            public void onSuccess(Channel result) {
                int code = result.showapi_res_code;
                List<Channel.ShowapiResBodyEntity.ChannelListEntity> channelList = result.showapi_res_body.channelList;
                if(channelList.size()==0){
                    mView.showError("",null, LoadView.EMPTY);
                    return;
                }
                if(code==0){
                    mMvpView.loadData(channelList);
                }else {
                    mMvpView.showInfo(result.showapi_res_error);
                }
            }
                },false);
    }




}
