package com.lq.fastnews.presenter;

import com.lq.fastnews.iview.ListMvpView;
import com.lq.fastnews.model.api.ApiDefine;
import com.lq.fastnews.model.bean.TodyHistory;
import com.lq.fastnews.model.subscribe.ListSubsribe;

/**
 * Created by Administrator on 2016/6/29.
 */
public class HistoryPresenter extends LoadPresenter<ListMvpView<TodyHistory.ResultBean>> {
    public void queryHistory(String m,String d){
        addObservable(getApiService().queryTodayHistory(ApiDefine.TODAY_HISTORY_KEY, "1.0", m, d), new ListSubsribe<TodyHistory,ListMvpView>(mMvpView) {

            @Override
            public void onSuccess(TodyHistory result) {
                int code = result.error_code;

            }
        },true);
    }
}
