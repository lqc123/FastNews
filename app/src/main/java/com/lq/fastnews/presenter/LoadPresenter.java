package com.lq.fastnews.presenter;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.model.api.ApiService;
import com.lq.fastnews.presenter.ipresenter.ILoadPresenter;
import com.lq.fastnews.utils.RxManager;
import com.lq.fastnews.iview.LoadView;

import rx.Observable;
import rx.Subscriber;


public class LoadPresenter<V extends LoadView> extends BasePresenter<V> implements ILoadPresenter {



    public void addObservable(Observable o, Subscriber s,boolean isLoadding){
        if(isLoadding)
        mMvpView.showLoading();
        RxManager.instance().add(o,s);
    }
    public ApiService getApiService(){
        return MyApp.get().getAppComponent().getApiService();
    }

}

