package com.lq.fastnews.presenter;

import com.lq.fastnews.iview.WeatherView;
import com.lq.fastnews.model.api.ApiDefine;
import com.lq.fastnews.model.subscribe.BaseSubsribe;
import com.lq.fastnews.model.bean.weather.WeatherResult;

/**
 * Created by Administrator on 2016/5/22.
 */
public class WeatherPresenter extends LoadPresenter<WeatherView> {


    public void loadData(final String cityName) {
     addObservable(getApiService().queryWeather(ApiDefine.API_KEY,cityName),new BaseSubsribe<WeatherResult,WeatherView>(mMvpView) {
                    @Override
                    public void onSuccess(WeatherResult result) {
                        int code = result.errNum;
                        mMvpView.hideLoading();

                        if(code==0){
                            mMvpView.loadData(result);
                        }else {
                            mMvpView.showInfo(result.errMsg);
                        }
                    }

                },true);
    }
}
