package com.lq.fastnews.presenter;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import com.lq.fastnews.iview.ListMvpView;
import com.lq.fastnews.model.api.ApiDefine;
import com.lq.fastnews.model.subscribe.ListSubsribe;
import com.lq.fastnews.model.bean.Result;
import com.lq.fastnews.model.bean.news.NewsInfo;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/5/6 0006.
 */
public class NewListPresenter extends LoadPresenter<ListMvpView<NewsInfo>> {

    public NewListPresenter(){

    }
    public void loadData(String channelId, String channeiName, final int page){
     addObservable(getApiService().queryNewsList(ApiDefine.API_KEY,channelId,channeiName,page),new ListSubsribe<Result,ListMvpView>(mMvpView) {
         @Override
         public void onSuccess(Result result) {

             JsonObject o = result.showapi_res_body;

             JsonObject pagebean = o.getAsJsonObject("pagebean");
             JsonArray jsonArray = pagebean.getAsJsonArray("contentlist");
             ArrayList<NewsInfo> newsInfos = new ArrayList<>();
             for (Object object : jsonArray) {
                 newsInfos.add(new Gson().fromJson(object.toString(), NewsInfo.class));
             }
             int code = result.showapi_res_code;

             if (page == 1) {
                 mMvpView.refresh(newsInfos);
             } else {
                 mMvpView.loadMore(newsInfos);
             }

//                        }else {
//                            mMvpView.showError(result.showapi_res_error,null);
//                        }
         }


     } ,false);
    }
}
