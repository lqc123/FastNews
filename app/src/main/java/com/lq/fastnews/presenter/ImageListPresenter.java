package com.lq.fastnews.presenter;

import com.lq.fastnews.model.api.ApiService;
import com.lq.fastnews.iview.ListMvpView;
import com.lq.fastnews.model.subscribe.ListSubsribe;
import com.lq.fastnews.model.bean.image.ImageInfo;
import com.lq.fastnews.model.bean.image.ImageResult;

import java.util.List;

/**
 * Created by Administrator on 2016/5/10 0010.
 */
public class ImageListPresenter extends LoadPresenter<ListMvpView<ImageInfo>> {
    public void loadData(String col,final int page) {
        int pn;
        if (page == 1) {
            pn = page;
        } else {
            pn = (page - 1) * 20;
        }
       addObservable( getApiService().queryImageList(col, ApiService.TAG, pn, ApiService.FROM, ApiService.RN),new ListSubsribe<ImageResult,ListMvpView>(mMvpView) {
                    @Override
                    public void onSuccess(ImageResult result) {
                        List<ImageInfo> imgs = result.imgs;
                        if(page==1){
                            mMvpView.refresh(imgs);
                        }else {

                            mMvpView.loadMore(imgs);
                        }
                    }

                },false);

    }
}
