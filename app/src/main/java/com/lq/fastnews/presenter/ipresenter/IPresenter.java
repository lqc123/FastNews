package com.lq.fastnews.presenter.ipresenter;


import com.lq.fastnews.iview.BaseView;

public interface IPresenter<V extends BaseView> {

    void attachView(V mvpView);

    void detachView();
    boolean isViewAttached();
    void checkViewAttached();
}
