package com.lq.fastnews.presenter;
import com.lq.fastnews.iview.BaseView;

import com.lq.fastnews.presenter.ipresenter.IPresenter;
import com.lq.fastnews.utils.RxManager;

public class BasePresenter<V extends BaseView> implements IPresenter<V> {
    protected V mMvpView;

    @Override
    public void attachView(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void detachView() {
        RxManager.instance().unsubscribe();
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call IPresenter.attachView(BaseMvpView) before" +
                    " requesting data to the IPresenter");
        }
    }
}
