package com.lq.fastnews.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lq.fastnews.R;
import com.lq.fastnews.model.bean.City;
import com.lq.fastnews.ui.activity.WeatherActivity;
import com.lq.fastnews.utils.MySP;

import java.util.List;

/**
 * Created by Administrator on 2016/6/4.
 */
public class CityAdapter extends  RecyclerView.Adapter<BaseViewHolder>   {
    private List<City> mList;
    private Activity mActivity;


    public CityAdapter(List<City> list, Activity act) {
        super();
        mList = list;

        mActivity = act;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
       final City city = mList.get(position);
        if (holder instanceof CityHolder) {
            ((CityHolder) holder).city_name.setText(city.name);
            holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MySP.saveData(MySP.CITY,city.name);
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();

                }
            });
        }else {
            String letter = city.pys.substring(0, 1);
            ((PinnedHolder) holder).city_tip.setText(letter);
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    protected int getDefItemViewType(int position) {
        City city = mList.get(position);
        return city.type;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0)
            return new CityHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false));
        else
            return new PinnedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pinned_header, parent, false));
    }


    @Override
    public int getItemViewType(int position) {
       return mList.get(position).type;

    }

    public int getLetterPosition(String letter){
        for (int i = 0 ; i < mList.size(); i++){
            if(mList.get(i).type ==1 && mList.get(i).pys.equals(letter)){
                return i;
            }
        }
        return -1;
    }

    class CityHolder extends BaseViewHolder {

        TextView city_name;

        public CityHolder(View view) {
            super(view);
            city_name = getView(R.id.city_name);
        }
    }


    class PinnedHolder extends BaseViewHolder {

        TextView city_tip;

        public PinnedHolder(View view) {
            super(view);
            city_tip = getView(R.id.city_tip);
        }
    }

}
