package com.lq.fastnews.ui.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.lq.fastnews.R;
import com.lq.fastnews.presenter.ImageListPresenter;
import com.lq.fastnews.ui.adapter.ImageListAdapter;
import com.lq.fastnews.ui.adapter.base.BaseSuperAdapter;
import com.lq.fastnews.utils.Const;

/**
 * Created by Administrator on 2016/5/9.
 */
public class ImageListFragment extends BaseListFragment<ImageListPresenter>{


    private String title;

    @NonNull
    @Override
    protected BaseSuperAdapter getListAdapter() {
        return new ImageListAdapter(mNews,R.layout.item_image,mActivity);
    }

    @Override
    protected void loadData() {
        title = getArguments().getString(Const.IMAGE_TITLE);
        mPresenter.loadData(title,mPage);
    }

    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        return manager;
    }

    @Override
    protected ImageListPresenter createPresenter() {
        return new ImageListPresenter();
    }

}
