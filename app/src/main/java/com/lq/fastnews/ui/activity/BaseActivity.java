package com.lq.fastnews.ui.activity;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.lq.fastnews.R;
import com.lq.fastnews.iview.BaseView;
import com.lq.fastnews.utils.StatusBarUtil;
import com.lq.fastnews.utils.SystemBarTintManager;
import com.lq.fastnews.utils.UIUtil;
import com.umeng.message.PushAgent;

import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/6/29 0029.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PushAgent.getInstance(this).onAppStart();
        //        锁定屏幕方向
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initViewPre();
        if (getContentViewLayoutID() != 0) {
            setContentView(getContentViewLayoutID());
            ButterKnife.bind(this);

        } else {
            throw new IllegalArgumentException("You must return a right contentView layout resource Id");
        }

        init();

    }

    protected void initViewPre() {

    }


    protected abstract void init();

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }



    protected abstract int getContentViewLayoutID();


    protected void setToolbar(Toolbar toolbar, String title, int bottomColor) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.arrows_normal);
//        RxToolbar.navigationClicks(toolbar).subscribe(new Action1<Void>() {
//            @Override
//            public void call(Void aVoid) {
//                onBackPressed();
//            }
//        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if(bottomColor>=0)
        setBottomNavigationColor(bottomColor);
    }

    protected void setBottomNavigationColor(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(id);
        }
    }
    protected void setBottomNavigationColor() {
        SystemBarTintManager mTintManager = new SystemBarTintManager(this);
        mTintManager.setNavigationBarTintEnabled(true);
        mTintManager.setStatusBarTintEnabled(true);
        int color = Color.argb(80, Color.red(0), Color.green(0), Color.blue(0));
        mTintManager.setTintColor(color);
    }



    /**
     * set status bar translucency
     *
     * @param on
     */
    protected void setTranslucentStatus(boolean on) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window win = getWindow();
            WindowManager.LayoutParams winParams = win.getAttributes();
            final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
            if (on) {
                winParams.flags |= bits;
            } else {
                winParams.flags &= ~bits;
            }
            win.setAttributes(winParams);
        }
    }

    /**
     * use SystemBarTintManager
     */
    public void setStatusBarColor(boolean on) {
        if (on) {
            StatusBarUtil.setColor(this, UIUtil.getRes().getColor(R.color.colorAccent), 0);
        }
    }



    @Override
    public void showInfo(String msg) {
        UIUtil.showToast(msg);
    }
}
