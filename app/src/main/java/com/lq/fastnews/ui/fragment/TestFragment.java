package com.lq.fastnews.ui.fragment;

import android.widget.TextView;

import com.lq.fastnews.R;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/5/6 0006.
 */
public class TestFragment extends BaseFragment {

    @Bind(R.id.tv)
    TextView tv;

    @Override
    protected void init() {


    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_test;
    }

}
