package com.lq.fastnews.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.lq.fastnews.R;
import com.lq.fastnews.utils.Const;
import com.lq.fastnews.utils.ImageLoadProxy;
import com.lq.fastnews.utils.ImageLoadUtil;
import com.lq.fastnews.utils.UIUtil;
import com.lq.fastnews.utils.XutilHelper;

import java.lang.ref.WeakReference;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/5/12 0012.
 */
public class DetailActivity extends LoadActivity implements Runnable {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.iv_detail_head)
    ImageView iv_detail_head;
    @Bind(R.id.ctl)
    CollapsingToolbarLayout ctl;
    @Bind(R.id.nest)
    NestedScrollView nest;
    @Bind(R.id.fabButton)
    FloatingActionButton fabButton;

     WebView mWebView;
    private String url;
    private String urlImage;


    @Override
    protected void init() {
        Intent intent = getIntent();
        String title = intent.getStringExtra(Const.TITLE);
        url = intent.getStringExtra(Const.URL);
        urlImage = intent.getStringExtra(Const.URL_IMAGE);
        setToolbar(toolbar,title, Color.TRANSPARENT);
        setBottomNavigationColor();

        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nest.smoothScrollTo(0, 0);
            }
        });
        if(urlImage !=null){
            ImageLoadUtil.loadNewsHead(this,urlImage,iv_detail_head);
        }
        initWebView();
    }

    @Override
    protected void initViewPre() {

    }

    private void initWebView() {
        WeakReference<Context> r = new WeakReference<Context>(this);
        Context a = r.get();
        mWebView = new WebView(a);
        nest.addView(mWebView,new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        //详细内容，显示的是html的内容；

        WebSettings ws = mWebView.getSettings();
        ws.setJavaScriptEnabled(true);
        ws.setAllowFileAccess(true);
        ws.setBuiltInZoomControls(false);
        ws.setSupportZoom(false);

/**
 *  设置网页布局类型：
 *  1、LayoutAlgorithm.NARROW_COLUMNS ： 适应内容大小
 *  2、LayoutAlgorithm.SINGLE_COLUMN: 适应屏幕，内容将自动缩放
 *
 */
        ws.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        ws.setDefaultTextEncodingName("utf-8"); //设置文本编码
        ws.setAppCacheEnabled(true);
        ws.setCacheMode(WebSettings.LOAD_DEFAULT);//设置缓存模式</span>


        ws.setLoadWithOverviewMode(true);
        ws.setUseWideViewPort(true);

//        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
//        settings.setJavaScriptEnabled(true);
//        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        settings.setDefaultTextEncodingName("UTF-8");
//        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//        settings.setBuiltInZoomControls(true);
//        settings.setSupportMultipleWindows(true);
//        settings.setUseWideViewPort(true);
//        settings.setLoadWithOverviewMode(true);
//        settings.setSupportZoom(true);
//        settings.setDomStorageEnabled(true);
//        settings.setLoadsImagesAutomatically(true);
//        System.out.println("---------------"+getIntent().getStringExtra(Const.TEXT));
//        String s = WebUtil.BuildHtmlWithCss(getIntent().getStringExtra(Const.TEXT),urlImage);
//        System.out.println("--------------------------"+s);
//        mWebView.loadDataWithBaseURL("",s, WebUtil.MIME_TYPE, WebUtil.ENCODING,"");
//        mWebView.loadData(s,WebUtil.MIME_TYPE,WebUtil.ENCODING);
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
// Uri uri = Uri.parse(url);
//        String scheme = uri.getScheme();
//         if (scheme.equals("http") || scheme.equals("https")) {
//            BrowserActivity.startActivity(getContext(), url);
//        }
        mWebView.loadUrl(url);

        mWebView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("url="+url);
                // TODO Auto-generated method stub
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
//                view.setWebChromeClient(wvcc);
                return true;
            }
        });
// 设置setWebChromeClient对象
        mWebView.setWebChromeClient(wvcc);
//        XutilHelper.postDelayed(this,Const.TIME);
//        return false;
    }
    WebChromeClient wvcc = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            System.out.println(title);
//            setToolbar(toolbar,title, Color.TRANSPARENT);
            toolbar.setTitle(title);
            UIUtil.showSnackbar(title,view);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            System.out.println(newProgress);
        }

    };
    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_detail;
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        //webview内存泄露
        if (mWebView != null) {
            ((ViewGroup) mWebView.getParent()).removeView(mWebView);
            mWebView.destroy();
            mWebView = null;
        }
        XutilHelper.removeCallbacks(this);
    }


    @Override
    public void run() {


    }
}
