package com.lq.fastnews.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.lq.fastnews.R;
import com.lq.fastnews.model.bean.image.ImageInfo;
import com.lq.fastnews.utils.ImageLoadProxy;
import com.lq.fastnews.utils.ImageLoadUtil;
import com.lq.fastnews.utils.UIUtil;

import java.util.Collection;
import java.util.List;

/**
 * Created by Administrator on 2016/5/13.
 */
public class CardAdapter extends BaseAdapter {

    private final Context mContext;
    List<ImageInfo> objs;


    public CardAdapter(List<ImageInfo> list, Context context) {
        objs = list;
        mContext = context;
    }

    public void addAll(Collection<ImageInfo> collection) {
        if (isEmpty()) {
            objs.addAll(collection);
            notifyDataSetChanged();
        } else {
            objs.addAll(collection);
        }
    }

    public void clear() {
        objs.clear();
        notifyDataSetChanged();
    }

    public boolean isEmpty() {
        return objs.isEmpty();
    }

    public void remove(int index) {
        if (index > -1 && index < objs.size()) {
            objs.remove(index);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getCount() {
        return objs.size();
    }

    @Override
    public ImageInfo getItem(int position) {
        if(objs==null ||objs.size()==0) return null;
        return objs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // TODO: getView
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageInfo talent = getItem(position);
        if (convertView == null)
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_new_item, parent, false);
        ViewHolder holder = new ViewHolder();
        convertView.setTag(holder);
        DisplayMetrics dm = UIUtil.getRes().getDisplayMetrics();
        float density = dm.density;
        int cardWidth = (int) (dm.widthPixels - (2 * 18 * density));
        int cardHeight = (int) (dm.heightPixels - (338 * density));
        convertView.getLayoutParams().width = cardWidth;
        holder.portraitView = (ImageView) convertView.findViewById(R.id.portrait);
        //holder.portraitView.getLayoutParams().width = cardWidth;
        holder.portraitView.getLayoutParams().height = cardHeight;

        holder.nameView = (TextView) convertView.findViewById(R.id.name);
        //parentView.getLayoutParams().width = cardWidth;
        //holder.jobView = (TextView) convertView.findViewById(R.id.job);
        //holder.companyView = (TextView) convertView.findViewById(R.id.company);
        holder.cityView = (TextView) convertView.findViewById(R.id.city);
        holder.eduView = (TextView) convertView.findViewById(R.id.education);
        holder.workView = (TextView) convertView.findViewById(R.id.work_year);
        holder.collectView = (CheckedTextView) convertView.findViewById(R.id.favorite);
        ImageLoadUtil.displayImageList(mContext,talent.thumbLargeUrl,holder.portraitView);

//            holder.portraitView.setImageURI(Uri.parse(talent.thumbLargeUrl));
//            holder.nameView.setText(String.format("%s", talent.nickname));
//            //holder.jobView.setText(talent.jobName);
//
//            final CharSequence no = "暂无";
//
//            holder.cityView.setHint(no);
//            holder.cityView.setText(talent.cityName);
//            holder.cityView.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.home01_icon_location,0,0);
//
//            holder.eduView.setHint(no);
//            holder.eduView.setText(talent.educationName);
//            holder.eduView.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.home01_icon_edu,0,0);
//
//            holder.workView.setHint(no);
//            holder.workView.setText(talent.workYearName);
//            holder.workView.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.home01_icon_work_year,0,0);


        return convertView;
    }



    private static class ViewHolder {
    ImageView portraitView;
    TextView nameView;
    TextView cityView;
    TextView eduView;
    TextView workView;
    CheckedTextView collectView;

    }
}

