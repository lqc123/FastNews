package com.lq.fastnews.ui.activity;

import android.view.View;

import com.lq.fastnews.iview.LoadView;
import com.lq.fastnews.ui.dialog.WaitDialog;

/**
 * Created by Administrator on 2016/4/17.
 */
public abstract  class LoadActivity extends BaseActivity implements LoadView {

    private WaitDialog mWaitDialog;





    @Override
    protected void onDestroy() {
        super.onDestroy();
        hideLoading();
    }



    @Override
    public void showLoading() {
        if(mWaitDialog==null){
            mWaitDialog = new WaitDialog(this);
            mWaitDialog.setCancelable(false);
        }
       if(mWaitDialog!=null&&!mWaitDialog.isShowing()){
           mWaitDialog.show();
       }

    }

    @Override
    public void hideLoading() {
        if(mWaitDialog!=null&&mWaitDialog.isShowing()){
            mWaitDialog.dismiss();
        }
    }

    @Override
    public void showError(String msg, View.OnClickListener onClickListener, int type) {

    }


    @Override
    public void restore() {

    }
}
