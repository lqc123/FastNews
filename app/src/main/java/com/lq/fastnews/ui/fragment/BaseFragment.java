package com.lq.fastnews.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.lq.fastnews.iview.LoadView;
import com.lq.fastnews.presenter.LoadPresenter;
import com.lq.fastnews.utils.LogUtil;
import com.lq.fastnews.utils.UIUtil;

import com.lq.fastnews.ui.view.loading.VaryViewHelperController;

import butterknife.ButterKnife;

/**
 * Created by Administrator on 2016/5/4 0004.
 */
public abstract  class BaseFragment<P extends LoadPresenter> extends Fragment implements LoadView {

    protected FragmentActivity mActivity;
    protected View mContentView;
    private VaryViewHelperController mVaryViewHelperController;
    private boolean isFirstInvisible;

    protected P mPresenter;
    protected boolean isFristVisible=true;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // 避免多次从xml中加载布局文件
        if (mContentView == null) {
            mContentView = LayoutInflater.from(mActivity).inflate(getLayoutId(), null);

        } else {
            ViewParent parent = mContentView.getParent();
            if (parent != null && parent instanceof ViewGroup) {
                ViewGroup group = (ViewGroup) parent;
                group.removeView(mContentView);
            }
        }
        return mContentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

            mPresenter = createPresenter();
            init();
            if (null != getLoadingTargetView()) {
                mVaryViewHelperController = new VaryViewHelperController(getLoadingTargetView());
            }
            if(mPresenter!=null){
                mPresenter.attachView(this);
            }


    }



    protected P createPresenter() {
        return null;
    }

    protected abstract void init();



    protected abstract int getLayoutId();




      View getLoadingTargetView(){
        return null;
    }




    @Override
    public void onDestroyView() {

        super.onDestroyView();
//        if(mContentView!=null)
//        ((ViewGroup) mContentView.getParent()).removeView(mContentView);
        if(mPresenter!=null){
            mPresenter.detachView();
        }
        ButterKnife.unbind(this);
    }
    protected FragmentManager getSupportFragmentManager() {
        return mActivity.getSupportFragmentManager();
    }

    @Override
    public void showLoading() {
        if(mVaryViewHelperController!=null)
        mVaryViewHelperController.showLoading();
    }

    @Override
    public void hideLoading() {
        if(mVaryViewHelperController!=null)
        mVaryViewHelperController.restore();
    }

    @Override
    public void showError(String msg, View.OnClickListener onClickListener, int type) {
        if (mVaryViewHelperController==null){
            return;
        }
        switch (type){
            case LoadView.EMPTY:
                mVaryViewHelperController.showEmpty(msg,onClickListener);
                break;
            case LoadView.ERROR:
                mVaryViewHelperController.showError(msg,onClickListener);
                break;
            case LoadView.NET_ERROR:
                mVaryViewHelperController.showNetworkError(onClickListener);
                break;
            default:
                break;
        }
    }

    @Override
    public void showInfo(String msg) {
        UIUtil.getSnackbar(msg,mContentView);
    }

    @Override
    public void restore() {
        if(mVaryViewHelperController!=null)
        mVaryViewHelperController.restore();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(isFristVisible){
                isFristVisible=false;
                userFristVisible();
            }
            userVisible();
        }else {
            userInvisible();
        }
    }
private final String TAG=getClass().getName();
    protected void userInvisible() {
        LogUtil.i(TAG,"userInvisible");
    }


    protected void userVisible() {
        LogUtil.i(TAG,"userVisible");
    }

    protected void userFristVisible() {
        LogUtil.i(TAG,"userFristVisible");
    }

}
