package com.lq.fastnews.ui.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.lq.fastnews.R;
import com.lq.fastnews.ui.adapter.pager.ImageAdapter;
import com.lq.fastnews.utils.Const;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/6/29 0029.
 */
public class ImageActivity extends BaseActivity {

    @Bind(R.id.tab)
    TabLayout tab;
    @Bind(R.id.vp)
    ViewPager vp;
    @Bind(R.id.ll_tab)
    View ll_tab;


    @Override
    protected void init() {
        String[] titles = Const.imageTitles;
//        vp.setOffscreenPageLimit(titles.length-1);
        vp.setAdapter(new ImageAdapter(getSupportFragmentManager(),titles));
        tab.setupWithViewPager(vp);
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.fragment_news_list;
    }
}
