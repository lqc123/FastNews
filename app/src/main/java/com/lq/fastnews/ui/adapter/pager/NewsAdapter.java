package com.lq.fastnews.ui.adapter.pager;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.model.bean.Channel;
import com.lq.fastnews.ui.fragment.ImageFragment;
import com.lq.fastnews.ui.fragment.NewsListFragment;
import com.lq.fastnews.utils.Const;

import java.util.List;

/**
 * Created by Administrator on 2016/5/23 0023.
 */
public class NewsAdapter extends FragmentStatePagerAdapter {
    private List<Channel.ShowapiResBodyEntity.ChannelListEntity> mTitles;
    public NewsAdapter(FragmentManager fm, List<Channel.ShowapiResBodyEntity.ChannelListEntity> title) {
        super(fm);
        this.mTitles = title;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(Const.CHANNEL,mTitles.get(position));
        Fragment fragment = Fragment.instantiate(MyApp.mContext, NewsListFragment.class.getName(), bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return mTitles.size();

    }
//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position).name;
    }
//    @Override
//    public int getItemPosition(Object object) {
//        return POSITION_NONE;
//    }
}
