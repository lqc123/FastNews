package com.lq.fastnews.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lq.fastnews.R;
import com.lq.fastnews.model.subscribe.TaskSubsriber;
import com.lq.fastnews.model.bean.City;
import com.lq.fastnews.ui.adapter.CityAdapter;
import com.lq.fastnews.utils.LetterComparator;
import com.lq.fastnews.ui.view.recycler.PinnedHeaderDecoration;
import com.lq.fastnews.utils.RxManager;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import cc.solart.wave.WaveSideBarView;
import rx.Observable;
import rx.functions.Func1;


/**
 * Created by Administrator on 2016/6/4.
 */
public class CityActivity extends LoadActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.side_view)
    WaveSideBarView sideView;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private CityAdapter adapter;


    @Override
    protected void init() {
        setToolbar(toolbar,"城市选择",-1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        final PinnedHeaderDecoration decoration = new PinnedHeaderDecoration();
        decoration.registerTypePinnedHeader(1, new PinnedHeaderDecoration.PinnedHeaderCreator() {
            @Override
            public boolean create(RecyclerView parent, int adapterPosition) {
                return true;
            }
        });
        recyclerView.addItemDecoration(decoration);
        Observable<List<City>> map = Observable.just(City.DATA).map(new Func1<String, List<City>>() {
            @Override
            public List<City> call(String s) {

                Type listType = new TypeToken<ArrayList<City>>() {
                }.getType();
                Gson gson = new Gson();
                final List<City> list = gson.fromJson(s, listType);
                Collections.sort(list, new LetterComparator());
                return list;
            }
        });
        RxManager.instance().add(map, new TaskSubsriber<List<City>>() {

            @Override
            public void onNext(List<City> cities) {
                adapter = new CityAdapter(cities,CityActivity.this);
                recyclerView.setAdapter(adapter);
            }
        });

        sideView.setOnTouchLetterChangeListener(new WaveSideBarView.OnTouchLetterChangeListener() {
            @Override
            public void onLetterChange(String letter) {
                int pos = adapter.getLetterPosition(letter);

                if (pos != -1) {
                    recyclerView.scrollToPosition(pos);
                }
            }
        });
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_city;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RxManager.instance().unsubscribe();
    }
}
