package com.lq.fastnews.ui.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.lq.fastnews.utils.UIUtil;

/**
 * Created by Administrator on 2016/8/19 0019.
 */
public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        UIUtil.showToast(intent.getAction());
    }
}
