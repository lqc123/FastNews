package com.lq.fastnews.ui.fragment;

import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.R;
import com.lq.fastnews.iview.ListMvpView;
import com.lq.fastnews.presenter.LoadPresenter;
import com.lq.fastnews.ui.adapter.BaseViewHolder;
import com.lq.fastnews.ui.adapter.base.BaseSuperAdapter;
import com.lq.fastnews.utils.Const;
import com.lq.fastnews.utils.ImageLoadProxy;
import com.lq.fastnews.utils.ImageLoadUtil;
import com.lq.fastnews.utils.MyViewUtils;
import com.lq.fastnews.utils.XutilHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import me.fangx.haorefresh.HaoRecyclerView;
import me.fangx.haorefresh.LoadMoreListener;

/**
 * Created by Administrator on 2016/5/5 0005.
 */
public abstract class BaseListFragment<P extends LoadPresenter>  extends BaseFragment<P> implements ListMvpView<List>, SwipeRefreshLayout.OnRefreshListener, Runnable {
    @Bind(R.id.recycler)
    HaoRecyclerView recycler;
    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;
    protected int mPage;
    protected List mNews = new ArrayList<>();
    private BaseSuperAdapter mAdapter;

    @Override
    protected void init() {
        MyViewUtils.setRefreshView(refresh);
        MyViewUtils.setRecyclerView(recycler,true,true,getLayoutManager());
        //设置自定义加载中和到底了效
        TextView tv = new TextView(mActivity);
        tv.setText("加载中...");
        recycler.setFootLoadingView(tv);
        TextView textView = new TextView(mActivity);
        textView.setText("已经到底啦~");
        recycler.setFootEndView(textView);
        mAdapter = getListAdapter();
        recycler.setAdapter(mAdapter);
        recycler.setRecyclerListener(new RecyclerView.RecyclerListener() {
            @Override
            public void onViewRecycled(RecyclerView.ViewHolder holder) {
                if(holder instanceof BaseViewHolder){
                    BaseViewHolder baseViewHolder=(BaseViewHolder)holder;
                    ImageLoadUtil.clear(baseViewHolder.getView(R.id.iv));
                }

            }
        });
        recycler.setLoadMoreListener(new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                initHttp(Const.LOAD);
            }
        });
        refresh.setOnRefreshListener(this);
    }



    @NonNull
    protected abstract BaseSuperAdapter getListAdapter();



    @Override
    protected void userFristVisible() {
        super.userFristVisible();
        XutilHelper.postDelayed(this,Const.LONG_TIME);
    }
    public void initHttp(int type) {
        if(type==Const.LOAD){
           mPage++;
        }else {
            mPage=1;
        }
        loadData();
    }

    protected abstract void loadData();



    @Override
    protected int getLayoutId() {
        return R.layout.view_refresh_recycler;
    }

    @Override
    protected View getLoadingTargetView() {
        return refresh;
    }


    @Override
    public void refresh(List data) {
        mNews.clear();
        mNews.addAll(data);
        recycler.refreshComplete();
        recycler.loadMoreComplete();
//        refresh.setRefreshing(false);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void loadMore(List data) {
        if (data==null||data.size()==0) {
//            recycler.loadMoreEnd();
        } else {
            mNews.addAll(data);
            recycler.loadMoreComplete();
            mAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onRefresh() {
        initHttp(Const.INIT);
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(MyApp.mContext);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mNews!=null){
            mNews.clear();
            mNews=null;
        }
        XutilHelper.removeCallbacks(this);
        ImageLoadUtil.clearMemory();
    }

    @Override
    public void clearLoad() {

        if(refresh!=null){
            refresh.setRefreshing(false);
        }
        if(recycler!=null)
            recycler.loadMoreEnd();
    }


    @Override
    public void run() {
        if(mPresenter!=null&&refresh!=null){
//            refresh.setRefreshing(true);
            showLoading();
            initHttp(Const.INIT);
        }
    }




}
