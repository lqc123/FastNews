package com.lq.fastnews.ui.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.lq.crad.SwipeFlingAdapterView;
import com.lq.fastnews.R;
import com.lq.fastnews.model.bean.image.ImageInfo;
import com.lq.fastnews.ui.activity.MainActivity;
import com.lq.fastnews.ui.adapter.CardAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 2016/5/13.
 */
public class CardDialog extends DialogFragment implements SwipeFlingAdapterView.onFlingListener {
    SwipeFlingAdapterView swipe_view;
    public static final String CARD_LIST="CARD_LIST";
    public static final String CARD_POSITION="CARD_POSITION";
    private CardAdapter adapter;

    public static CardDialog instance(int num, ArrayList<ImageInfo> list){
        CardDialog dialog = new CardDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(CARD_POSITION,num);
        bundle.putSerializable(CARD_LIST,list);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.R.style.Theme_Dialog, R.style.AppTheme);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setWindowAnimations(R.style.animate_card);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode==KeyEvent.KEYCODE_BACK){
                    dismissAllowingStateLoss();
                    ((MainActivity)getActivity()).showRevealAnim(null,false);
                }
                return false;
            }
        });
        View view =  inflater.inflate(R.layout.view_card, container, false);
        swipe_view= (SwipeFlingAdapterView) view.findViewById(R.id.swipe_view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        }
        return dialog;
    }
    private void initView() {
        Bundle bundle = getArguments();
        int i = bundle.getInt(CARD_POSITION);
        ArrayList<ImageInfo> list = (ArrayList<ImageInfo>) bundle.getSerializable(CARD_LIST);
        ArrayList<ImageInfo> infos = (ArrayList<ImageInfo>) list.clone();
//        for(ImageInfo info:list){
//            infos.add(info);
//        }
//        infos.
        Collections.copy(infos,list);
        swipe_view.setIsNeedSwipe(true);
        swipe_view.setFlingListener(this);
//        swipe_view.setOnItemClickListener(this);

        adapter = new CardAdapter(infos,getActivity());


        swipe_view.setAdapter(adapter);
//        swipe_view.setSelection(i);
    }

    @Override
    public void removeFirstObjectInAdapter() {
        adapter.remove(0);
    }

    @Override
    public void onLeftCardExit(Object dataObject) {

    }

    @Override
    public void onRightCardExit(Object dataObject) {

    }

    @Override
    public void onAdapterAboutToEmpty(int itemsInAdapter) {

    }

    @Override
    public void onScroll(float progress, float scrollXProgress) {

    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();

        if(swipe_view!=null){
            swipe_view.cancel();
            swipe_view=null;
        }
    }
}
