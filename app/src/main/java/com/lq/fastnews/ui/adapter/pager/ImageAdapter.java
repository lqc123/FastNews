package com.lq.fastnews.ui.adapter.pager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.ui.fragment.ImageListFragment;
import com.lq.fastnews.utils.Const;

/**
 * Created by Administrator on 2016/5/23 0023.
 */
public class ImageAdapter  extends FragmentStatePagerAdapter {
    private String[] titles;
    public ImageAdapter(FragmentManager fm,String[] title) {
        super(fm);
        titles=title;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(Const.IMAGE_TITLE,titles[position]);
        return Fragment.instantiate(MyApp.mContext,ImageListFragment.class.getName(),bundle);
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }

//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//    }
}
