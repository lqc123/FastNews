package com.lq.fastnews.ui.adapter.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import com.lq.fastnews.MyApp;
import com.lq.fastnews.ui.fragment.NewsFragment;
import com.lq.fastnews.ui.fragment.TestFragment;

/**
 * Created by Administrator on 2016/5/23 0023.
 */
public class HomeAdapter extends FragmentStatePagerAdapter {
    /**
     * Fragment数组界面
     *
     */
    private Class mFragmentArray[] = {NewsFragment.class,TestFragment.class,TestFragment.class};
    private FragmentManager mFragmentManager;

    public HomeAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
    }

    @Override
    public Fragment getItem(int position) {
        return     Fragment.instantiate(MyApp.mContext,mFragmentArray[position].getName());
    }



    @Override
    public int getCount() {
        return mFragmentArray.length;
    }
}
