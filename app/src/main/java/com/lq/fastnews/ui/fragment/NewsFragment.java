package com.lq.fastnews.ui.fragment;

import android.view.View;

import com.lq.fastnews.iview.NetMvpView;
import com.lq.fastnews.iview.NewsView;
import com.lq.fastnews.model.bean.Channel;
import com.lq.fastnews.presenter.NewsPresenter;

import com.lq.fastnews.ui.adapter.pager.NewsAdapter;
import com.lq.fastnews.utils.XutilHelper;

import java.util.List;

/**
 * Created by Administrator on 2016/5/5 0005.
 */
public class NewsFragment extends BaseTabFragment<NewsPresenter> implements NewsView<List<Channel.ShowapiResBodyEntity.ChannelListEntity>>, Runnable {


    @Override
    protected void init() {


    }


    @Override
    protected View getLoadingTargetView() {
        return vp;
    }

    @Override
    public void loadData(List<Channel.ShowapiResBodyEntity.ChannelListEntity> data) {
//        vp.setOffscreenPageLimit(4);
        vp.setAdapter(new NewsAdapter(getChildFragmentManager(),data));
        tab.setupWithViewPager(vp);
    }

    @Override
    protected void userVisible() {
        super.userVisible();
        XutilHelper.postDelayed(this,200);
    }

    @Override
    protected NewsPresenter createPresenter() {

        return new NewsPresenter();
    }

    @Override
    public void run() {
        if(mPresenter!=null)mPresenter.newsTitle();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        XutilHelper.removeCallbacks(this);
    }

    @Override
    public void reload() {
        run();
    }
}
