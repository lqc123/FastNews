package com.lq.fastnews.ui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lq.fastnews.R;
import com.lq.fastnews.iview.WeatherView;
import com.lq.fastnews.model.bean.weather.WeatherResult;
import com.lq.fastnews.presenter.WeatherPresenter;
import com.lq.fastnews.utils.MySP;
import com.lq.fastnews.utils.StatusBarUtil;
import com.lq.fastnews.utils.WeatherHelp;


import butterknife.Bind;
import butterknife.OnClick;

public class WeatherActivity extends LoadActivity implements WeatherView {
    public static final int CITY_REQUEST=1;
    @Bind(R.id.tv_city)
    TextView tvCity;

    @Bind(R.id.iv_now_weather)
    ImageView ivNowWeather;
    @Bind(R.id.tv_now_weather)
    TextView tvNowWeather;
    @Bind(R.id.tv_today_temp)
    TextView tvTodayTemp;
    @Bind(R.id.tv_now_temp)
    TextView tvNowTemp;
    @Bind(R.id.tv_wind_scale)
    TextView tv_wind_scale;

    @Bind(R.id.iv_today_weather)
    ImageView ivTodayWeather;
    @Bind(R.id.tv_today_temp_a)
    TextView tvTodayTempA;
    @Bind(R.id.tv_today_temp_b)
    TextView tvTodayTempB;
    @Bind(R.id.tv_tommorrow)
    TextView tvTommorrow;
    @Bind(R.id.iv_tommorrow_weather)
    ImageView ivTommorrowWeather;
    @Bind(R.id.tv_tommorrow_temp_a)
    TextView tvTommorrowTempA;
    @Bind(R.id.tv_tommorrow_temp_b)
    TextView tvTommorrowTempB;
    @Bind(R.id.tv_thirdday)
    TextView tvThirdday;
    @Bind(R.id.iv_thirdday_weather)
    ImageView ivThirddayWeather;
    @Bind(R.id.tv_thirdday_temp_a)
    TextView tvThirddayTempA;
    @Bind(R.id.tv_thirdday_temp_b)
    TextView tvThirddayTempB;
    @Bind(R.id.tv_fourthday)
    TextView tvFourthday;
    @Bind(R.id.iv_fourthday_weather)
    ImageView ivFourthdayWeather;
    @Bind(R.id.tv_fourthday_temp_a)
    TextView tvFourthdayTempA;
    @Bind(R.id.tv_fourthday_temp_b)
    TextView tvFourthdayTempB;
    @Bind(R.id.tv_felt_air_temp)
    TextView tvFeltAirTemp;
    @Bind(R.id.tv_humidity)
    TextView tvHumidity;
    @Bind(R.id.tv_wind)
    TextView tvWind;
    @Bind(R.id.tv_uv_index)
    TextView tvUvIndex;
    @Bind(R.id.tv_dressing_index)
    TextView tvDressingIndex;
    @Bind(R.id.tv_fiveday)
    TextView tv_fiveday;
    @Bind(R.id.iv_fiveday_weather)
    ImageView iv_fiveday_weather;
    @Bind(R.id.tv_fiveday_temp_a)
    TextView tv_fiveday_temp_a;
    @Bind(R.id.tv_fiveday_temp_b)
    TextView tv_fiveday_temp_b;



    private WeatherPresenter mWeatherPresenter;

    @Override
    protected void init() {
        StatusBarUtil.setTranslucent(this,1);
        setBottomNavigationColor();
        mWeatherPresenter = new WeatherPresenter();
        mWeatherPresenter.attachView(this);
        mWeatherPresenter.loadData(MySP.getCity());
    }
    @OnClick(R.id.back)
    public void back(View v){
        finish();
    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.activity_weather;
    }

    @Override
    public void loadData(WeatherResult result) {
        tvCity.setText(result.retData.city);
        tvTodayTempA.setText(result.retData.today.lowtemp);
        tvTodayTempB.setText(result.retData.today.hightemp);
        tvTommorrow.setText(result.retData.forecast.get(0).date+"    "+result.retData.forecast.get(0).type);
        tvTommorrowTempA.setText(result.retData.forecast.get(0).lowtemp);
        tvTommorrowTempB.setText(result.retData.forecast.get(0).hightemp);
        tvThirdday.setText(result.retData.forecast.get(1).date+"    "+result.retData.forecast.get(1).type);
        tvThirddayTempA.setText(result.retData.forecast.get(1).lowtemp);
        tvThirddayTempB.setText(result.retData.forecast.get(1).hightemp);
//        tvThirdday.setText(result.retData.forecast.get(2).date);
//        tvThirddayTempA.setText(result.retData.forecast.get(2).lowtemp);
//        tvThirddayTempB.setText(result.retData.forecast.get(2).hightemp);
        tvFourthday.setText(result.retData.forecast.get(2).date+"    "+result.retData.forecast.get(2).type);
        tvFourthdayTempA.setText(result.retData.forecast.get(2).lowtemp);
        tvFourthdayTempB.setText(result.retData.forecast.get(3).hightemp);
        tv_fiveday.setText(result.retData.forecast.get(3).date+"    "+result.retData.forecast.get(3).type);
        tv_fiveday_temp_a.setText(result.retData.forecast.get(3).lowtemp);
        tv_fiveday_temp_b.setText(result.retData.forecast.get(3).hightemp);

        tvHumidity.setText(result.retData.today.index.get(1).index);
        tvWind.setText(result.retData.today.index.get(2).index);
        tvUvIndex.setText(result.retData.today.index.get(3).index);
        tvDressingIndex.setText(result.retData.today.index.get(4).index);
        tvNowWeather.setText(result.retData.today.type);
        tvNowTemp.setText(result.retData.today.curTemp);
        tvTodayTemp.setText(result.retData.today.lowtemp+" - "+result.retData.today.hightemp);
        tv_wind_scale.setText(result.retData.today.fengxiang+"   "+result.retData.today.fengli);
        setWeatherIcon(ivTommorrowWeather,result.retData.forecast.get(0).type);
        setWeatherIcon(ivThirddayWeather,result.retData.forecast.get(1).type);
        setWeatherIcon(ivFourthdayWeather,result.retData.forecast.get(2).type);
        setWeatherIcon(iv_fiveday_weather,result.retData.forecast.get(3).type);

        setWeatherIcon(ivTodayWeather,result.retData.today.type);
        setWeatherIcon(ivNowWeather,result.retData.today.type);


    }
    @OnClick(R.id.tv_city)
    void city( ){
//        UIUtil.startActivity(this,CityActivity.class);
        startActivityForResult(new Intent(this,CityActivity.class),CITY_REQUEST);
    }


    private void setWeatherIcon(ImageView image,String type){
        image.setImageResource(WeatherHelp.getWeatherType().get(type));
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mWeatherPresenter!=null) mWeatherPresenter.detachView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==CITY_REQUEST&&resultCode==RESULT_OK){
            mWeatherPresenter.loadData(MySP.getCity());
            System.out.println("--------------------------------------");
        }
    }
}
