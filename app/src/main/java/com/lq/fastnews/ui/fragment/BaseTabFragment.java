package com.lq.fastnews.ui.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.lq.fastnews.R;
import com.lq.fastnews.presenter.LoadPresenter;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public abstract class BaseTabFragment<P extends LoadPresenter>  extends BaseFragment<P>{
    @Bind(R.id.tab)
    TabLayout tab;
    @Bind(R.id.vp)
    ViewPager vp;
    @Bind(R.id.ll_tab)
    View ll_tab;
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_news_list;
    }

}
