package com.lq.fastnews.ui.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.lq.fastnews.R;
import com.lq.fastnews.model.bean.Channel;
import com.lq.fastnews.presenter.NewListPresenter;
import com.lq.fastnews.ui.adapter.NewListAdapter;
import com.lq.fastnews.utils.Const;

/**
 * Created by Administrator on 2016/5/5 0005.
 */
public  class NewsListFragment extends BaseListFragment<NewListPresenter>{

    private Channel.ShowapiResBodyEntity.ChannelListEntity channel;

    @NonNull
    @Override
    protected NewListAdapter getListAdapter() {

            return new NewListAdapter(mNews, R.layout.item_news,mActivity);

    }

    @Override
    protected NewListPresenter createPresenter() {
        return new NewListPresenter();
    }

    @Override
    protected void loadData() {
        if(channel==null)
        channel = (Channel.ShowapiResBodyEntity.ChannelListEntity) getArguments().getSerializable(Const.CHANNEL);
        mPresenter.loadData(channel.channelId,channel.name,mPage);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
