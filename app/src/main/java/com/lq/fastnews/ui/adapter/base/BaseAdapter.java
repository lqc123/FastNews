package com.lq.fastnews.ui.adapter.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.lq.fastnews.ui.adapter.BaseViewHolder;

import java.lang.ref.WeakReference;

/**
 * Created by Administrator on 2016/3/4 0004.
 */
public abstract class  BaseAdapter<VH extends BaseViewHolder> extends RecyclerView.Adapter<VH> {


    @Override
    public VH onCreateViewHolder(ViewGroup parent, final int viewType) {
        final VH holder = onCreate(parent, viewType);
        return holder;
    }

    protected abstract VH onCreate(ViewGroup parent, int viewType);
}
