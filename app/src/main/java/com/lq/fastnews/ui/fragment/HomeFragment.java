package com.lq.fastnews.ui.fragment;

import android.support.v4.view.ViewPager;

import com.lq.fastnews.R;
import com.lq.fastnews.ui.adapter.pager.HomeAdapter;

import butterknife.Bind;
import me.majiajie.pagerbottomtabstrip.Controller;
import me.majiajie.pagerbottomtabstrip.PagerBottomTabLayout;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectListener;

/**
 * Created by Administrator on 2016/5/5 0005.
 */
public class HomeFragment extends BaseFragment implements OnTabItemSelectListener {




    /**
     * 选修卡文字
     *
     */
    private static String mTitle[] = { "首页", "资讯", "我的" };


    @Bind(R.id.view_pager)
     ViewPager mViewPager;
    @Bind(R.id.tab)
    PagerBottomTabLayout tab;


    @Override
    protected void init() {
        initBottomTab();
        mViewPager.setOffscreenPageLimit(mTitle.length);
        mViewPager.setAdapter(new HomeAdapter(getChildFragmentManager()));
        initEvent();
        mViewPager.setCurrentItem(0);
    }
    private void initBottomTab() {

        //用TabItemBuilder构建一个导航按钮
//        TabItemBuilder tabItemBuilder = new TabItemBuilder(getContext()).create()
//                .setDefaultIcon(android.R.drawable.ic_menu_send)
//                .setText("信息")
//                .setSelectedColor(0xFF00C29B)
//                .setTag("A")
//                .build();

        //构建导航栏,得到Controller进行后续控制
        Controller controller = tab.builder()
                .addTabItem(R.mipmap.ic_tab_home, mTitle[0], 0xFF00C29B)
                .addTabItem(R.mipmap.ic_tab_zx, mTitle[1], 0xFF00C29B)
                .addTabItem(R.mipmap.ic_tab_my, mTitle[2], 0xFF00C29B)
//                .setMode(TabLayoutMode.HIDE_TEXT)
//                .setMode(TabLayoutMode.CHANGE_BACKGROUND_COLOR)
//                .setMode(TabLayoutMode.HIDE_TEXT| TabLayoutMode.CHANGE_BACKGROUND_COLOR)
                .build();

        controller.addTabItemClickListener(this);


    }

    private void initEvent() {


    }







    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onSelected(int index, Object tag) {
        mViewPager.setCurrentItem(index);
    }

    @Override
    public void onRepeatClick(int index, Object tag) {

    }



}
