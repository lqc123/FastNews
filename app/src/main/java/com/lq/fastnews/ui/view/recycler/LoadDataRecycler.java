package com.lq.fastnews.ui.view.recycler;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lq.fastnews.R;

/**
 * Created by Administrator on 2016/4/17.
 */
public class LoadDataRecycler<T> implements SwipeRefreshLayout.OnRefreshListener{

    private final Context mContext;
    private View view;

    public LoadDataRecycler(Context context,ViewGroup container){
        mContext = context;
        init(container);
    }

    private void init(ViewGroup container) {
        view = LayoutInflater.from(mContext).inflate(R.layout.view_refresh_recycler, container, false);
        View recycler = view.findViewById(R.id.recycler);
    }

    @Override
    public void onRefresh() {

    }
}
