package com.lq.fastnews.ui.activity;


import com.lq.crad.SwipeFlingAdapterView;
import com.lq.fastnews.R;

import butterknife.Bind;

/**
 * Created by Administrator on 2016/5/13.
 */
public class CardActivity extends LoadActivity {
    @Bind(R.id.swipe_view)
    SwipeFlingAdapterView swipe_view;
    @Override
    protected void init() {

    }

    @Override
    protected int getContentViewLayoutID() {
        return R.layout.view_card;
    }
}
