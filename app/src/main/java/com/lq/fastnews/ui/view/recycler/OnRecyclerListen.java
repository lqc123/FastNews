package com.lq.fastnews.ui.view.recycler;

import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.lq.fastnews.MyApp;

/**
 * Created by Administrator on 2016/8/19 0019.
 */
public class OnRecyclerListen implements RecyclerView.RecyclerListener {

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        Glide.clear(holder.itemView);
    }
}
