package com.lq.fastnews.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.RequestManager;
import com.lq.fastnews.R;
import com.lq.fastnews.model.bean.news.HeadImageInfo;
import com.lq.fastnews.model.bean.news.NewsInfo;
import com.lq.fastnews.ui.activity.DetailActivity;
import com.lq.fastnews.ui.adapter.base.BaseSuperAdapter;
import com.lq.fastnews.utils.Const;

import java.util.List;

/**
 * Created by Administrator on 2016/5/8.
 */
public class NewListAdapter extends BaseSuperAdapter<NewsInfo> {
    private Context mContext;


    public NewListAdapter(List<NewsInfo> list, int layoutResId, Context context) {
        super(list, layoutResId);
        mContext=context;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        final NewsInfo newsInfo = mList.get(position);
        holder.setText(R.id.tv_title,newsInfo.title);
        holder.setText(R.id.tv_desc,newsInfo.desc);
        holder.setText(R.id.tv_date,newsInfo.source+"  "+newsInfo.pubDate);


//        holder.setImageUri(,R.id.iv,getUrl(newsInfo.imageurls));

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra(Const.TITLE,newsInfo.title);
                intent.putExtra(Const.URL,newsInfo.link);
                Object o = newsInfo.allList;
                if(o!=null){
                    intent.putExtra(Const.TEXT,o.toString());
                }
                intent.putExtra(Const.URL_IMAGE,getUrl(newsInfo.imageurls));

                mContext.startActivity(intent);
            }
        });

    }

    private String getUrl(List<HeadImageInfo> imageurls){
        if(imageurls!=null&&imageurls.size()!=0){
            return imageurls.get(0).url;
        }
        return "";
    }
}
