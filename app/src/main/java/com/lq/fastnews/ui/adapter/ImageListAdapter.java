package com.lq.fastnews.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jakewharton.rxbinding.view.RxView;
import com.lq.fastnews.R;
import com.lq.fastnews.model.bean.image.ImageInfo;
import com.lq.fastnews.ui.activity.MainActivity;
import com.lq.fastnews.ui.adapter.base.BaseSuperAdapter;
import com.lq.fastnews.ui.dialog.CardDialog;
import com.lq.fastnews.utils.RxManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.codetail.animation.SupportAnimator;
import rx.functions.Action1;

/**
 * Created by Administrator on 2016/5/10 0010.
 */
public class ImageListAdapter extends BaseSuperAdapter<ImageInfo> {
    private Context mContext;


    public ImageListAdapter(List<ImageInfo> list, int layoutResId, Context context) {
        super(list, layoutResId);
        mContext = context;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, final int position) {
        final ImageInfo imageInfo = mList.get(position);
        holder.setImageUri(mContext,R.id.iv,imageInfo.thumbLargeUrl);
//        ImageView imageView = holder.getView(R.id.item_img);
//        mRequestManager.load(imageInfo.thumbLargeUrl).placeholder(R.mipmap.bg).error(R.mipmap.bg).
        RxManager.instance().fastClick(holder.itemView,new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                final MainActivity a = (MainActivity) ImageListAdapter.this.mContext;
                a.showRevealAnim(new SupportAnimator.AnimatorListener() {
                    @Override
                    public void onAnimationStart() {

                    }

                    @Override
                    public void onAnimationEnd() {
                        CardDialog dialog = CardDialog.instance(position, (ArrayList<ImageInfo>) mList);
                        dialog.show(a.getFragmentManager(),"");
                    }

                    @Override
                    public void onAnimationCancel() {

                    }

                    @Override
                    public void onAnimationRepeat() {

                    }
                }, true);
            }
        });
//        holder.setOnClickListener(new FastOnClicklistener(){
//            @Override
//            public void onClick(View v) {
//
//                super.onClick(v);
////               imageInfo.thumbLargeUrl;
//                final MainActivity a = (MainActivity) ImageListAdapter.this.mContext;
//                a.showRevealAnim(new SupportAnimator.AnimatorListener() {
//                    @Override
//                    public void onAnimationStart() {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd() {
//                        CardDialog dialog = CardDialog.instance(position, (ArrayList<ImageInfo>) mList);
//                        dialog.show(a.getFragmentManager(),"");
//                    }
//
//                    @Override
//                    public void onAnimationCancel() {
//
//                    }
//
//                    @Override
//                    public void onAnimationRepeat() {
//
//                    }
//                }, true);
//            }
//        });
    }






}
