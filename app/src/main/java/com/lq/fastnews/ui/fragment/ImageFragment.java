package com.lq.fastnews.ui.fragment;

import com.lq.fastnews.ui.adapter.pager.ImageAdapter;
import com.lq.fastnews.utils.Const;

/**
 * Created by Administrator on 2016/5/11 0011.
 */
public class ImageFragment extends BaseTabFragment {

    @Override
    protected void init() {
         String[] titles = Const.imageTitles;
//        vp.setOffscreenPageLimit(titles.length-1);
        vp.setAdapter(new ImageAdapter(getChildFragmentManager(),titles));
        tab.setupWithViewPager(vp);
    }

}
