package com.lq.fastnews.ui.adapter.base;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lq.fastnews.ui.adapter.BaseViewHolder;

import java.util.List;

/**
 * Base of SuperAdapter.
 * Created by Cheney on 15/11/28.
 */
public abstract class BaseSuperAdapter<T> extends  RecyclerView.Adapter<BaseViewHolder>  {;

    protected int mLayoutResId;

    protected List<T> mList;


    public BaseSuperAdapter( List<T> list, int layoutResId) {
        this.mList = list ;
        this.mLayoutResId = layoutResId;
    }




    public List<T> getList() {
        return mList;
    }



    @Override
    public int getItemCount() {

        return mList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutResId, parent, false);
        return new BaseViewHolder(view);
    }
    public void add(T item) {
        mList.add(item);
        int index = mList.size() - 1;

        notifyItemInserted(index);
    }

    public void insert(int index, T item) {
        mList.add(index, item);

        notifyItemInserted(index);
    }

    public void addAll(List<T> items) {
        if (items == null || items.size() == 0) {
            return;
        }
        int start = mList.size();
        mList.addAll(items);
        notifyItemRangeInserted(start, items.size());
    }

    public void remove(T item) {
        if (mList.contains(item)) {
            int index = mList.indexOf(item);
            remove(index);
        }
    }

    public void remove(int index) {
        mList.remove(index);

        notifyItemRemoved(index);
    }

    public void set(T oldItem, T newItem) {
        set(mList.indexOf(oldItem), newItem);
    }

    public void set(int index, T item) {
        mList.set(index, item);

        notifyItemChanged(index);
    }

    public void replaceAll(List<T> items) {
        clear();
        addAll(items);
    }

    public boolean contains(T item) {
        return mList.contains(item);
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onViewRecycled(BaseViewHolder holder) {
        super.onViewRecycled(holder);
        System.out.println("---------------------回收了");
    }
}
