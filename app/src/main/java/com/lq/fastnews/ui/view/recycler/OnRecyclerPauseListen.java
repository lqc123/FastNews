package com.lq.fastnews.ui.view.recycler;

import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by 95 on 2016/1/5.
 */
public class OnRecyclerPauseListen extends RecyclerView.OnScrollListener {
    private final boolean pauseOnScroll;
    private final boolean pauseOnFling;
    private final RecyclerView.OnScrollListener externalListener;

    public OnRecyclerPauseListen( boolean pauseOnScroll, boolean pauseOnFling) {
        this(pauseOnScroll, pauseOnFling, (RecyclerView.OnScrollListener)null);
    }

    public OnRecyclerPauseListen( boolean pauseOnScroll, boolean pauseOnFling, RecyclerView.OnScrollListener customListener) {
        this.pauseOnScroll = pauseOnScroll;
        this.pauseOnFling = pauseOnFling;
        this.externalListener = customListener;
    }

    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        switch(newState) {
            case 0:
                Glide.with(recyclerView.getContext()).resumeRequests();
                break;
            case 1:
                if(this.pauseOnScroll) {
                    Glide.with(recyclerView.getContext()).pauseRequests();
                }
                break;
            case 2:
                if(this.pauseOnFling) {
                    Glide.with(recyclerView.getContext()).pauseRequests();
                }
                break;
        }

        if(this.externalListener != null) {
            this.externalListener.onScrollStateChanged(recyclerView, newState);
        }

    }

    public void onScrolled(RecyclerView recyclerView,int dx, int dy) {
        if(this.externalListener != null) {
            this.externalListener.onScrolled(recyclerView,dx,dy);
        }

    }


}
