package com.lq.fastnews;

import android.app.Application;
import android.content.Context;


import com.lq.fastnews.model.api.ApiServiceModule;
import com.lq.fastnews.model.api.AppComponent;

import com.lq.fastnews.model.api.DaggerAppComponent;
import com.lq.fastnews.utils.ImageLoadProxy;
import com.squareup.leakcanary.LeakCanary;


/**
 * Created by Administrator on 2016/4/17.
 */
public class MyApp extends Application{
    public  static Context mContext;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext=getApplicationContext();
        /**
         * 内存溢出检测工具
         */
        LeakCanary.install(this);
        setDraggerConfig();
        ImageLoadProxy.initImageLoader(mContext);
        System.out.println("---------------------------init");
    }
    public static MyApp get() {
        return (MyApp) mContext.getApplicationContext();
    }

    /**
     * 初始化Dragger，DaggerAppComponent是自动生成，需要Rebuild
     */
    private void setDraggerConfig() {
        appComponent = DaggerAppComponent.builder()
                .apiServiceModule(new ApiServiceModule())
                .build();
    }
    public AppComponent getAppComponent() {
        return appComponent;
    }
}
