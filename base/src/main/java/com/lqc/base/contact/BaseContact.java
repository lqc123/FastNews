package com.lqc.base.contact;

import android.view.View;

/**
 * Created by Administrator on 2016/6/18.
 */
public interface BaseContact {
    interface BaseView{
        void showLoading(String msg);

        void hideLoading();

        void showError(String msg, View.OnClickListener onClickListener);

        void showEmpty(String msg, View.OnClickListener onClickListener);

        void showNetError(View.OnClickListener onClickListener);
    }
    interface BasePresenter<V extends BaseView> {
        void attachView(V mvpView);
        void detachView();
    }
}
